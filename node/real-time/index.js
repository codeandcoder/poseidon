var db = require('../db');
var utils = require('../utils');
var chat = require('./chat/index.js');

var pageSockets = {};
var userSockets = {};

exports.init = function(io) {
    io.sockets.on('connection', function(socket) {
        var onevent = socket.onevent;
        socket.onevent = function (packet) {
            var args = packet.data || [];
            onevent.call (this, packet);    // original call
            packet.data = ["*"].concat(args);
            onevent.call(this, packet);      // additional call to catch-all
        };

        /*socket.on("*",function(event, data) {
            var session_token = utils.parseCookies(socket.request.headers.cookie).session_token;
            if (session_token) {
                db.checkSession(session_token, function(err, user) {
                    if (!err && user) {
                        
                    }
                });
            }
        });*/

        socket.on('page-changed', function(data) {
            socket.page = data.page;
            if (socket.user) {
                if (data.page == '/chat') {
                    chat.userEnteredChat(socket.user);
                } else {
                    chat.userExitedChat(socket.user);
                }
            }
            
        });

        socket.on('request-session', function(data) {
            if (data.session_token) {
                db.checkSession(data.session_token, function(err, user) {
                    if (err) return socket.emit('session', {err: err});
                    if (user && !socket.token) {
                        socket.token = utils.genToken();
                        socket.user = user;
                        pageSockets[socket.page] = !pageSockets[socket.page] ? {} : pageSockets[socket.page];
                        pageSockets[socket.page][socket.token] = socket;
                        userSockets[socket.user._id] = !userSockets[socket.user._id] ? {} : userSockets[socket.user._id];
                        userSockets[socket.user._id][socket.token] = socket;
                        if (Object.keys(userSockets[socket.user._id]).length == 1) {
                            chat.connectUser(socket.user);
                        }
                        db.countPendingMessages(user._id, function(err, count){
                            socket.emit('pending-messages', {count: count});
                        });
                    } else if (socket.token) {
                        socket.user = user;
                        removeTokenInPages(socket.token);
                        pageSockets[socket.page] = !pageSockets[socket.page] ? {} : pageSockets[socket.page];
                        pageSockets[socket.page][socket.token] = socket;
                    }
                    return socket.emit('session', {user: user});
                });
            } else {
                return socket.emit('session', {});
            }
        });

        socket.on('request-logout', function(data) {
            if (socket.user) {
                db.removeSession(socket.user._id, function(err) {
                    sendToAllSockets(userSockets[socket.user._id], 'logout', {});
                    chat.disconnectUser(socket.user);
                    userSockets[socket.user._id] = {};
                    socket.user = undefined;
                    socket.token = undefined;
                });
            }
        });

        socket.on('disconnect', function() {
            if (socket.user) {
                if (socket.page == '/chat') {
                    chat.userExitedChat(socket.user);
                }
                delete userSockets[socket.user._id][socket.token];
                if (Object.keys(userSockets[socket.user._id]).length == 0) {
                    chat.disconnectUser(socket.user);
                    /*getConnectedUsers(function(err, result) {
                        if (err) {
                            logger.error(err);
                        } else {
                            connectedUsers = result.users;
                            //sendToAll('update-connected-users', {users:result.users});
                        }
                    });*/
                }
            }
            if (socket.page && pageSockets[socket.page] && pageSockets[socket.page][socket.token]) {
                delete pageSockets[socket.page][socket.token];
            }
            socket.token = undefined;
        });

        require('./users').config(socket);
        require('./blog').config(socket);
        chat.config(socket);
    });
    chat.init(pageSockets, userSockets, sendToAllSockets);
};

function sendToAllSockets(sockets, operation, data) {
    if (sockets) {
        var keys = Object.keys(sockets);
        for (var i = 0; i < keys.length; i++) {
            var s = sockets[keys[i]];
            s.emit(operation, data);
        }
    }
}

function removeTokenInPages(token) {
    var pageKeys = Object.keys(pageSockets);
    for (var i = 0; i < pageKeys.length; i++) {
        delete pageSockets[pageKeys[i]][token];
    }
}