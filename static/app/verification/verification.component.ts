import { Component, OnInit } from '@angular/core';
import { NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SocketService } from '../sockets/socket.service';

@Component({
  selector: 'amgs-verification',
  templateUrl: 'app/verification/verification.template.html',
  styleUrls: ['app/verification/verification.style.css'],
  providers: [],
  directives: []
})
export class VerificationComponent implements OnInit {
    error: any;
    successful = false;

    ngOnInit() {
        this.socketService.socket.on('session', (data:any) => {
            if (data.err) {
            } else if (data.user) {
                document.location.href = '/';
            } else {
                this.route.params.map(params => params['token']).subscribe((token) => {
                    this.socketService.send('request-verification', {token: token}, (data:any) => {
                        if (data.err) {
                            this.error = {
                                type: 'danger',
                                msg: 'No se ha podido verificar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                            }
                        } else {
                            this.successful = true;
                        }
                    });
                });
            }
        });
        this.socketService.socket.emit('request-session', {session_token: this.socketService.session_token, page: location.pathname});
    }

    constructor(private socketService: SocketService, private route: ActivatedRoute) {
    }
}