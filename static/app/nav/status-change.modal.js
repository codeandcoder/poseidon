"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var socket_service_1 = require('../sockets/socket.service');
var angular2_modal_1 = require('angular2-modal');
var index_1 = require('angular2-modal/plugins/bootstrap/index');
var CustomModalContext = (function (_super) {
    __extends(CustomModalContext, _super);
    function CustomModalContext() {
        _super.apply(this, arguments);
    }
    return CustomModalContext;
}(index_1.BSModalContext));
exports.CustomModalContext = CustomModalContext;
/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
var StatusChangeModal = (function () {
    function StatusChangeModal(dialog, socketService) {
        this.dialog = dialog;
        this.socketService = socketService;
        this.context = dialog.context;
    }
    StatusChangeModal.prototype.changeStatus = function (status) {
        this.socketService.socket.emit('update-chat-user', { status: status });
        this.dialog.close();
    };
    StatusChangeModal.prototype.beforeDismiss = function () {
        return true;
    };
    StatusChangeModal = __decorate([
        core_1.Component({
            selector: 'modal-content',
            template: "\n      <div style=\"margin: 10px;\">\n        <button class=\"btn btn-primary background-green content-white\" style=\"margin-right: 10px;\" (click)=\"changeStatus('connected')\">Conectado</button>\n        <button class=\"btn btn-primary background-red content-white\" style=\"margin-right: 10px;\" (click)=\"changeStatus('bussy')\">Ocupado</button>\n        <button class=\"btn btn-primary background-gold content-white\" style=\"margin-right: 10px;\" (click)=\"changeStatus('navigating')\">Ausente</button>\n        <button class=\"btn btn-primary background-grey content-white\" style=\"margin-right: 10px;\" (click)=\"changeStatus('disconnected')\">Desconectado</button>\n      </div>\n  "
        }), 
        __metadata('design:paramtypes', [angular2_modal_1.DialogRef, socket_service_1.SocketService])
    ], StatusChangeModal);
    return StatusChangeModal;
}());
exports.StatusChangeModal = StatusChangeModal;
//# sourceMappingURL=status-change.modal.js.map