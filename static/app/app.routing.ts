import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HomeCreationComponent } from './home-creation/home-creation.component';
import { HomeEditComponent } from './home-edit/home-edit.component';
import { ForumComponent } from './forum/forum.component';
import { ChairComponent } from './chair/chair.component';
import { UnknownComponent } from './unknown/unknown.component';
import { ShopComponent } from './shop/shop.component';
import { VerificationComponent } from './verification/verification.component';
import { ChatComponent } from './chat/chat.component';
import { TermsComponent } from './terms/terms.component';

const appRoutes: Routes = [
    {
        path: 'inicio',
        component: HomeComponent
    },
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'registro',
        component: RegisterComponent
    },
    {
        path: 'terms',
        component: TermsComponent
    },
    {
        path: 'verificar',
        component: VerificationComponent
    },
    {
        path: 'entrar',
        component: LoginComponent
    },
    {
        path: 'inicio-creacion',
        component: HomeCreationComponent
    },
    {
        path: 'inicio-edicion',
        component: HomeEditComponent
    },
    {
        path: 'foro',
        component: ForumComponent
    },
    {
        path: 'sillon',
        component: ChairComponent
    },
    {
        path: 'incognito',
        component: UnknownComponent
    },
    {
        path: 'tienda',
        component: ShopComponent
    },
    {
        path: 'chat',
        component: ChatComponent
    }
  /*{ path: 'crisis-center', component: CrisisCenterComponent },
  {
    path: 'heroes',
    component: HeroListComponent,
    data: {
      title: 'Heroes List'
    }
  },
  { path: 'hero/:id', component: HeroDetailComponent },
  { path: '**', component: PageNotFoundComponent }*/
];

export const appRoutingProviders: any[] = [

];

export const routing = RouterModule.forRoot(appRoutes);