import { Component, Inject, forwardRef, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';
import { AppComponent } from '../app.component';

import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { UsersService } from '../users/user.service';
import { MenuItem } from '../nav/menu-item.model';

@Component({
    selector: 'animagens-home',
    templateUrl: '/app/home/home.template.html',
    styleUrls: ['app/home/home.style.css'],
    directives: [NavComponent],
    providers: [],
    animations: [
        trigger('postState', [
            state('void', style({opacity: 0, transform: 'translateY(+200px)'})),
            state('in', style({opacity: 1, transform: 'translateY(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1,
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ]),
        trigger('backgroundState', [
            state('void', style({opacity: 0})),
            state('in', style({opacity: 1})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1
                    }))
                ])
            ])
        ])
    ]
})
export class HomeComponent {
    user: any;

    blogs: [any];
    users:any = this.usersService.users;

    constructor(private socketService: SocketService, private usersService: UsersService) {
        socketService.send('request-blogs', {}, (data:any) => {
            this.blogs = data.blogs;
            for (var i = 0; i < this.blogs.length; i++) {
                if (!usersService.users[this.blogs[i].user_id]) {
                    usersService.getUser(this.blogs[i].user_id, () => {});
                }
            }
        });
        socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});
        socketService.socket.on('session', (data:any) => {
            if (data.user) {
                this.user = data.user;
            } else {
                this.user = undefined;
            }
        });
    }
}