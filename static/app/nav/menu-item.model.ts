export class MenuItem {
    title: string;
    link: string;
    icon: string;
    color: string;
    active: boolean;

    constructor(title: string, link: string, icon: string, color: string, active: boolean) {
        this.title = title;
        this.link = link;
        this.icon = icon;
        this.color = color;
        this.active = active;
    }
}