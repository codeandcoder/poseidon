"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var angular2_recaptcha_1 = require('angular2-recaptcha/angular2-recaptcha');
var socket_service_1 = require('../sockets/socket.service');
var nav_component_1 = require('../nav/nav.component');
var login_model_1 = require('./login.model');
var LoginComponent = (function () {
    function LoginComponent(socketService, router) {
        this.socketService = socketService;
        this.router = router;
        this.model = new login_model_1.Login('', '');
        this.submitted = false;
        this.successful = false;
        socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
        socketService.socket.on('session', function (data) {
            if (data.user) {
                console.log('isLogin!!');
                router.navigate(['/']);
            }
        });
    }
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.error = null;
        this.submitted = true;
        this.socketService.send('request-login', this.model, function (data) {
            _this.submitted = false;
            if (!data.err) {
                document.cookie = "session_token=" + data.session_token;
                _this.socketService.session_token = data.session_token;
                _this.router.navigate(['/']);
            }
            else if (data.err.code == 109) {
                _this.error = {
                    type: 'danger',
                    msg: 'El usuario y/o la contraseña introducidos no son correctos'
                };
            }
            else if (data.err.code == 110) {
                _this.error = {
                    type: 'danger',
                    msg: 'El usuario introducido no ha sido verificado aún. Comprueba la bandeja de correo no deseado.'
                };
            }
            else {
                _this.error = {
                    type: 'danger',
                    msg: 'Error desconocido. Por favor, contacta con nosotros en soporte@animagens.es para arreglarlo lo antes posible'
                };
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'animagens-login',
            templateUrl: '/app/login/login.template.html',
            styleUrls: ['app/login/login.style.css'],
            directives: [nav_component_1.NavComponent, angular2_recaptcha_1.ReCaptchaComponent],
            providers: [],
            animations: [
                core_1.trigger('postState', [
                    core_1.state('void', core_1.style({ opacity: 0, transform: 'translateY(+200px)' })),
                    core_1.state('in', core_1.style({ opacity: 1, transform: 'translateY(0px)' })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1,
                                transform: 'translateY(0px)'
                            }))
                        ])
                    ])
                ]),
                core_1.trigger('backgroundState', [
                    core_1.state('void', core_1.style({ opacity: 0 })),
                    core_1.state('in', core_1.style({ opacity: 1 })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1
                            }))
                        ])
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map