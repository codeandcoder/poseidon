"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var SocketService = (function () {
    function SocketService() {
        this.request_id = 0;
        this.listeners = {};
        this.socket = io(config.socketUrl);
        this.session_token = parseCookies(document.cookie).session_token;
        this.clearListeners();
    }
    SocketService.prototype.send = function (operation, data, f) {
        this.listeners[this.request_id] = f;
        this.socket.emit(operation, { id: this.request_id, data: data });
        this.request_id++;
    };
    SocketService.prototype.clearListeners = function () {
        var _this = this;
        this.socket.removeAllListeners();
        this.socket.on('response', function (info) {
            _this.listeners[info.id](info.data);
            delete _this.listeners[info.id];
        });
        this.socket.on('disconnect', function (data) {
            location.reload();
        });
    };
    SocketService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], SocketService);
    return SocketService;
}());
exports.SocketService = SocketService;
function parseCookies(rc) {
    var list = {};
    rc && rc.split(';').forEach(function (cookie) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}
//# sourceMappingURL=socket.service.js.map