var db = require('../db');
var logger = require('../logger');
var utils = require('../utils');
var constants = require('../constants');
var enotifier = require('../enotifier');

exports.config = function(socket) {
    socket.on('request-blog', function(request) {
        db.findBlog(request.data.blog_id, function(err, blog) {
            socket.emit('response', {id:request.id, data:{blog: blog}});
        });
    });

    socket.on('request-blogs', function(request) {
        db.findBlogs(function(err, blogs) {
            socket.emit('response', {id:request.id, data:{blogs: blogs}});
        });
    });

    socket.on('create-blog', function(request) {
        var data = request.data;
        if (socket.user && socket.user.perms.writeBlog) {
            db.createBlog(data.title, data.content, data.image, socket.user._id, function(err) {
                socket.emit('response', {id:request.id, data:{err: err}});
            });
        } else {
            socket.emit('response', {id:request.id, data:{err: constants.getError('c112')}});
        }
    });

    socket.on('edit-blog', function(request) {
        var data = request.data;
        db.findBlog(data._id, function(err, blog) {
            if (socket.user && (socket.user.perms.isAdmin || socket.user._id == blog.user_id)) {
                db.editBlog(data._id, data.title, data.content, data.image, socket.user._id, function(err) {
                    socket.emit('response', {id:request.id, data:{err: err}});
                });
            } else {
                socket.emit('response', {id:request.id, data:{err: constants.getError('c112')}});
            }
        });
    });
};