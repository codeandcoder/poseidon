export class Blog {
  constructor(
    public image: string,
    public title: string,
    public content: string
  ) {  }
}