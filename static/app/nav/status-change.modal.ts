import { Component } from '@angular/core';
import { SocketService } from '../sockets/socket.service';

import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap/index';

export class CustomModalContext extends BSModalContext {
}

/**
 * A Sample of how simple it is to create a new window, with its own injects.
 */
@Component({
  selector: 'modal-content',
  template: `
      <div style="margin: 10px;">
        <button class="btn btn-primary background-green content-white" style="margin-right: 10px;" (click)="changeStatus('connected')">Conectado</button>
        <button class="btn btn-primary background-red content-white" style="margin-right: 10px;" (click)="changeStatus('bussy')">Ocupado</button>
        <button class="btn btn-primary background-gold content-white" style="margin-right: 10px;" (click)="changeStatus('navigating')">Ausente</button>
        <button class="btn btn-primary background-grey content-white" style="margin-right: 10px;" (click)="changeStatus('disconnected')">Desconectado</button>
      </div>
  `
})
export class StatusChangeModal implements CloseGuard, ModalComponent<CustomModalContext> {
  context: CustomModalContext;

  constructor(public dialog: DialogRef<CustomModalContext>, private socketService: SocketService) {
    this.context = dialog.context;
  }

  changeStatus(status: string) {
      this.socketService.socket.emit('update-chat-user', {status: status});
      this.dialog.close();
  }

  beforeDismiss(): boolean {
    return true;
  }
}
