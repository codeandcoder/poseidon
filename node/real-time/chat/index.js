var db = require('../../db');
var debug = require('debug')('chat');
var utils = require('../../utils');
var constants = require('../../constants');
var enotifier = require('../../enotifier');

var pageSockets = {};
var userSockets = {};
var sendToAllSockets;

var chatUsers = {};
var chatRooms = [];

exports.init = function(pSockets, uSockets, staSockets) {
    pageSockets = pSockets;
    userSockets = uSockets;
    sendToAllSockets = staSockets;

    db.findChatRooms(function (err, rooms) {
        chatRooms = rooms;
    });
};

exports.config = function(socket) {
    socket.on('request-chat-users', function(data) {
        return socket.emit('chat-users', {chatUsers: filterUsers()});
    });

    socket.on('request-chat-user', function(data) {
        if (socket.user) {
            return socket.emit('chat-user', {chatUser: chatUsers[socket.user._id]});
        }
    });

    socket.on('update-chat-user', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            chatUsers[socket.user._id].status = data.status;
            sendUsersUpdate(socket.user._id);
        }
    });

    socket.on('request-starreds', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.findStarredUsers(socket.user._id, function(err, starreds) {
                return socket.emit('starreds', {err: err, starreds: starreds});
            });
        }
    });

    socket.on('make-starred', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.setStarred(socket.user._id, data.user_id, function(err) {
                db.findStarredUsers(socket.user._id, function(err, starreds) {
                    return socket.emit('starreds', {err: err, starreds: starreds});
                });
            });
        }
    });

    socket.on('make-unstarred', function(data) {
        if (socket.user && chatUsers[socket.user._id]) {
            db.setUnstarred(socket.user._id, data.user_id, function(err) {
                db.findStarredUsers(socket.user._id, function(err, starreds) {
                    return socket.emit('starreds', {err: err, starreds: starreds});
                });
            });
        }
    });
    
    socket.on('message', function(data) {
        var message = {
            content: utils.parseMessage(data.message),
            type: data.type,
            target_id: data._id,
            sender_id: socket.user._id,
            created_at: Date.now()
        };
        if (data.type == 'user') {
            var targetSockets = userSockets[data._id] || {};
            var found = false;
            var keys = Object.keys(targetSockets);
            for (var i = 0; i < keys.length && !found; i++) {
                var s = targetSockets[keys[i]];
                if (s.page == '/chat') {
                    found = true;
                }
            }
            if (chatUsers[data._id] && found) {
                sendToAllSockets(targetSockets, 'message', message);
                db.saveChatMessage(message, function(data) {});
            } else {
                db.savePendingMessage(message, function(result) {
                    if (Object.keys(targetSockets).length > 0) {
                        db.countPendingMessages(data._id, function(err, count){
                            sendToAllSockets(targetSockets, 'pending-messages', {count: count});
                        });
                    }
                });
            }
            sendToAllSockets(userSockets[socket.user._id], 'message', message);
        }
    });

    socket.on('request-history', function(info) {
        var data = info.data;
        db.findChatMessages(data.sender_id, data.target_id, data.from, data.amount, function(err, messages) {
            return socket.emit('response', {id: info.id, data:messages});
        });
    });

    socket.on('request-pendings', function(info) {
        var data = info.data;
        db.retrievePendingMessages(socket.user._id, function(err, conversations) {
            socket.emit('response', {id: info.id, data:conversations});
            db.countPendingMessages(socket.user._id, function(err, count){
                sendToAllSockets(userSockets[socket.user._id], 'pending-messages', {count: count});
            });
        });
    });

    socket.on('create-room', function(info) {
        var data = info.data;
        if (socket.user) {
            db.countRoomsByUser(socket.user._id, function(err, count) {
                if (!err && count < socket.user.perms.createRooms) {
                    data.user_id = socket.user._id;
                    db.saveChatRoom(data, function(err) {
                        if (err) {
                            socket.emit('response', {id: info.id, data: {err: constants.getError('c112')}});
                        } else {
                            db.findChatRooms(function (err, rooms) {
                                chatRooms = rooms;
                                sendRoomsUpdate();
                            });
                            socket.emit('response', {id: info.id, data: {}});
                        }
                    });
                } else {
                    socket.emit('response', {id: info.id, data: {err: constants.getError('c112')}});
                }
            })
        }
    });
};

exports.connectUser = function(user) {
    if (!chatUsers[user._id]) {
        chatUsers[user._id] = {
            status: 'navigating'
        }
        sendUsersUpdate(user._id);
    }
    sendToAllSockets(userSockets[user._id], 'chat-rooms', {chatRooms: chatRooms});
};

exports.userEnteredChat = function(user) {
    if (chatUsers[user._id]) {
        if (chatUsers[user._id].status == 'navigating') {
            chatUsers[user._id].status = 'connected';
            sendUsersUpdate(user._id);
        }
        sendToAllSockets(userSockets[user._id], 'chat-rooms', {chatRooms: chatRooms});
    };
};

exports.userExitedChat = function(user) {
    if (user && chatUsers[user._id] && chatUsers[user._id].status == 'connected') {
        var found = false;
        var keys = Object.keys(userSockets[user._id]);
        for (var i = 0; i < keys.length && !found; i++) {
            var s = userSockets[user._id][keys[i]];
            if (s.page == '/chat') {
                found = true;
            }
        }
        if (!found) {
            chatUsers[user._id].status = 'navigating';
            sendUsersUpdate(user._id);
        }
    };
};

exports.disconnectUser = function(user) {
    delete chatUsers[user._id];
    sendUsersUpdate(user._id);
};

function prepareUserList(users) {
    var userList = [];
    var keys = Object.keys(users);
    for (var i = 0; i < keys.length; i++) {
        var u = {
            _id: keys[i],
            status: users[keys[i]].status
        }
        userList.push(u);
    }
    return userList;
}

function sendUsersUpdate(user_id) {
    var keys = Object.keys(chatUsers);
    for (var i = 0; i < keys.length; i++) {
        sendToAllSockets(userSockets[keys[i]], 'chat-users', {chatUsers: filterUsers()});
    }
    sendToAllSockets(userSockets[user_id], 'chat-user', {chatUser: chatUsers[user_id]});
}

function sendRoomsUpdate() {
    var keys = Object.keys(chatUsers);
    for (var i = 0; i < keys.length; i++) {
        sendToAllSockets(userSockets[keys[i]], 'chat-rooms', {chatRooms: chatRooms});
    }
}

function filterUsers() {
    var filteredUsers = {};
    var keys = Object.keys(chatUsers);
    for (var i = 0; i < keys.length; i++) {
        if (chatUsers[keys[i]].status != 'disconnected') {
            filteredUsers[keys[i]] = chatUsers[keys[i]];
        }
    }
    return filteredUsers;
}