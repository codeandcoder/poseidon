module.exports = {
  'html' : 'text/html',
  'css'  : 'text/css',
  'tag'  : 'text/html',
  'js'   : 'text/js',
  'ico'  : 'image/x-icon',
  'png'  : 'image/png',
  'svg'  : 'image/svg+xml'
}
