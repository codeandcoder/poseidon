/**
 * System configuration for Angular 2 apps
 * Adjust as necessary for your application needs.
 */
(function(global) {

  // map tells the System loader where to look for things
  var map = {
    'app':                        '/app', // 'dist',
    '@angular':                   '/node_modules/@angular',
    'angular2/core':              '/node_modules/@angular/core',
    'angular2-recaptcha':         '/node_modules/angular2-recaptcha',
    'angular2-modal':             'https://unpkg.com/angular2-modal@2.0.0-beta.12',
    'angular2-in-memory-web-api': '/node_modules/angular2-in-memory-web-api',
    'rxjs':                       '/node_modules/rxjs',
    'socket.io-client':           '/node_modules/socket.io-client',
    'ng2-file-upload':            '/node_modules/ng2-file-upload',
    'ng2-uploader':               '/node_modules/ng2-uploader',
    'ng2-ckeditor':               '/node_modules/ng2-ckeditor',
    'angular2-auto-scroll':       '/node_modules/angular2-auto-scroll',
    'reflect-metadata':           '/node_modules/reflect-metadata'
  };

  // packages tells the System loader how to load when no filename and/or no extension
  var packages = {
    'app':                        { main: 'main.js',  defaultExtension: 'js' },
    'rxjs':                       { defaultExtension: 'js' },
    'angular2-in-memory-web-api': { main: 'index.js', defaultExtension: 'js' },
    'angular2-recaptcha':         {defaultExtension: 'js'},
    'angular2-modal':              { defaultExtension: 'js', main: 'bundles/angular2-modal.umd' },
    'ng2-file-upload':            {main: 'ng2-file-upload.js', defaultExtension: 'js'},
    'ng2-uploader':               { defaultExtension: 'js'},
    'ng2-ckeditor':               {main: 'lib/CKEditor.js', defaultExtension: 'js'},
    'angular2-auto-scroll':       { main: 'lib/angular2-auto-scroll.directive.js', defaultExtension: 'js'},
    'reflect-metadata':           { main: 'Reflect.js', defaultExtension: 'js'}
  };

  var ngPackageNames = [
    'common',
    'compiler',
    'core',
    'forms',
    'router',
    'http',
    'platform-browser',
    'platform-browser-dynamic',
    'upgrade',
  ];

  // Individual files (~300 requests):
  function packIndex(pkgName) {
    packages['@angular/'+pkgName] = { main: 'index.js', defaultExtension: 'js' };
  }

  // Bundled (~40 requests):
  function packUmd(pkgName) {
    packages['@angular/'+pkgName] = { main: '/bundles/' + pkgName + '.umd.js', defaultExtension: 'js' };
  }

  var setPackageConfig = System.packageWithIndex ? packIndex : packUmd;

  // Add package entries for angular packages
  ngPackageNames.forEach(setPackageConfig);

  var config = {
    map: map,
    packages: packages
  };

  System.config(config);

})(this);
