"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var socket_service_1 = require('../sockets/socket.service');
var UsersService = (function () {
    function UsersService(socketService) {
        var _this = this;
        this.socketService = socketService;
        this.users = {};
        socketService.socket.on('user-updated', function (data) {
            _this.users[data.user._id] = data.user;
        });
    }
    UsersService.prototype.getUser = function (u_id, f) {
        var _this = this;
        if (!this.users)
            this.users = {};
        if (!this.users[u_id]) {
            this.socketService.send('request-user', { user_id: u_id }, function (data) {
                _this.users[data._id] = data;
                f(_this.users[u_id]);
            });
        }
        else {
            f(this.users[u_id]);
        }
    };
    UsersService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [socket_service_1.SocketService])
    ], UsersService);
    return UsersService;
}());
exports.UsersService = UsersService;
//# sourceMappingURL=user.service.js.map