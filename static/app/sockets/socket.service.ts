import { Injectable } from '@angular/core';

@Injectable()
export class SocketService {
    socket: any;
    session_token: string;
    request_id: number = 0;
    listeners: any = {};

    send(operation: string, data: any, f: Function) {
        this.listeners[this.request_id] = f;
        this.socket.emit(operation, {id: this.request_id, data: data});
        this.request_id++;
    }

    clearListeners() {
        this.socket.removeAllListeners();
        
        this.socket.on('response', (info: any) => {
            this.listeners[info.id](info.data);
            delete this.listeners[info.id];
        });
        
        this.socket.on('disconnect', (data: any) => {
            location.reload();
        });
    }

    constructor() {
        this.socket = io(config.socketUrl);

        this.session_token = parseCookies(document.cookie).session_token;

        this.clearListeners();
    }
}

function parseCookies (rc: string) : any {
    var list = {};

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}