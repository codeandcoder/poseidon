var constants = require('../constants');

exports.info = function(data) {
    var log = {
        type: 'INFO'
    };
	protectPassword(data, function() {
        log.data = data;
		console.log(log);
	});
};

exports.warn = function(data) {
    var log = {
        type: 'WARN'
    };
	protectPassword(data, function() {
        log.data = data;
		console.log(log);
	});
};

exports.error = function(data) {
    var log = {
        type: 'ERROR'
    };
	protectPassword(data, function() {
		if (data.errCode != undefined) {
			var fullError = constants.getError(data.errCode);
            log.data = fullError;
			console.error(log);
		} else {
            log.data = data;
			console.error(log);
		}
	});
};

function protectPassword(err, callback) {
	if (err.data !== undefined && err.data.password !== undefined) {
		err.data.password = '*********';
	}
	if (err.data !== undefined && err.data['rep-password'] !== undefined) {
		err.data['rep-password'] = '*********';
	}
	return callback(err);
}