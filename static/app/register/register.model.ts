export class Register {
  constructor(
    public nickname: string,
    public alias: string,
    public email: string,
    public rep_email: string,
    public password: string,
    public rep_password: string,
    public birth: string,
    public gender: string
  ) {  }
}