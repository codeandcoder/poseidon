"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var nav_component_1 = require('./nav/nav.component');
var menu_item_model_1 = require('./nav/menu-item.model');
var router_1 = require('@angular/router');
var socket_service_1 = require('./sockets/socket.service');
var AppComponent = (function () {
    function AppComponent(socketService, router) {
        var _this = this;
        this.socketService = socketService;
        this.pageColor = 'blue';
        this.menuItems = [
            new menu_item_model_1.MenuItem('Inicio', '/inicio', 'home', 'blue', true),
        ];
        this.windowFocused = true;
        window.onfocus = function (event) {
            _this.windowFocused = true;
        };
        window.onblur = function (event) {
            _this.windowFocused = false;
        };
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                _this.socketService.clearListeners();
            }
            if (event instanceof router_1.NavigationEnd) {
                _this.socketService.socket.emit('page-changed', { page: router.url });
                socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
                socketService.socket.on('session', function (data) {
                    if (data.user) {
                        _this.user = data.user;
                    }
                    else {
                        _this.user = undefined;
                    }
                });
                socketService.socket.on('logout', function (data) {
                    router.navigate(['/']);
                    socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
                });
            }
        });
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'animagens-app',
            template: "\n        <span defaultOverlayTarget></span>\n        <animagens-nav [user]=\"user\" [pageColor]=\"pageColor\" [menuItems]=\"menuItems\"></animagens-nav>\n        <router-outlet></router-outlet>\n    ",
            directives: [nav_component_1.NavComponent],
            providers: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map