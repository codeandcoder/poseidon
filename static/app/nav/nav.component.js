"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var router_1 = require('@angular/router');
var platform_browser_1 = require('@angular/platform-browser');
var angular2_modal_1 = require('angular2-modal');
var index_1 = require('angular2-modal/plugins/bootstrap/index');
var socket_service_1 = require('../sockets/socket.service');
var status_change_modal_1 = require('./status-change.modal');
var NavComponent = (function () {
    function NavComponent(socketService, zone, router, title, modal, overlay) {
        var _this = this;
        this.socketService = socketService;
        this.zone = zone;
        this.modal = modal;
        this.overlay = overlay;
        this.pendingMessages = 0;
        this.statuses = [
            'connected',
            'navigating',
            'bussy',
            'disconnected'
        ];
        this.colors = {
            'connected': 'green',
            'navigating': 'gold',
            'bussy': 'red',
            'disconnected': 'grey'
        };
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                socketService.socket.on('session', function (data) {
                    _this.socketService.socket.emit('request-chat-user', {});
                });
                _this.socketService.socket.on('chat-user', function (data) {
                    if (data.err) {
                        _this.myChatUser = {
                            status: 'navigating'
                        };
                    }
                    else {
                        _this.myChatUser = data.chatUser;
                    }
                });
                _this.socketService.socket.on('pending-messages', function (data) {
                    _this.pendingMessages = data.count;
                    if (_this.pendingMessages > 0) {
                        title.setTitle('(' + _this.pendingMessages + ') Animagens');
                    }
                    else {
                        title.setTitle('Animagens');
                    }
                });
            }
        });
    }
    NavComponent.prototype.getStatusTitle = function () {
        if (this.myChatUser) {
            switch (this.myChatUser.status) {
                case 'connected':
                    return 'Conectado';
                case 'navigating':
                    return 'Navegando';
                case 'bussy':
                    return 'Ocupado';
                case 'disconnected':
                    return 'Desconectado';
            }
        }
        return '';
    };
    NavComponent.prototype.openStatusModal = function () {
        return this.modal.open(status_change_modal_1.StatusChangeModal, angular2_modal_1.overlayConfigFactory({}, index_1.BSModalContext));
    };
    NavComponent.prototype.logout = function () {
        this.socketService.socket.emit('request-logout');
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], NavComponent.prototype, "user", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], NavComponent.prototype, "pageColor", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], NavComponent.prototype, "menuItems", void 0);
    NavComponent = __decorate([
        core_1.Component({
            selector: 'animagens-nav',
            templateUrl: '/app/nav/nav.template.html',
            styleUrls: ['app/nav/nav.style.css'],
            directives: [],
            providers: [],
            animations: [
                core_1.trigger('navbarState', [
                    core_1.state('void', core_1.style({ transform: 'translateY(-150px)' })),
                    core_1.state('in', core_1.style({ transform: 'translateX(0px)' })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                transform: 'translateY(0px)'
                            }))
                        ])
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, router_1.Router, platform_browser_1.Title, index_1.Modal, angular2_modal_1.Overlay])
    ], NavComponent);
    return NavComponent;
}());
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map