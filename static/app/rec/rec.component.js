"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var core_2 = require('@angular/core');
var router_deprecated_1 = require('@angular/router-deprecated');
var socket_service_1 = require('../sockets/socket.service');
var RecComponent = (function () {
    function RecComponent(socketService, zone, params, fb) {
        this.socketService = socketService;
        this.zone = zone;
        this.params = params;
        this.fb = fb;
        this.successful = false;
        this.submitted = false;
        this.user = { nickname: 'test' };
    }
    RecComponent.prototype.onSubmit = function (value) {
        this.error = undefined;
        if (value.rep_password != value.password) {
            this.error = {
                type: 'warning',
                msg: 'Las passwords no coinciden.'
            };
        }
        else {
            this.submitted = true;
            this.socketService.send('update-rec-user-password', {
                token: this.params.get('token'),
                password: value.password
            });
        }
        return true;
    };
    RecComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.form = this.fb.group({
            password: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(7)])],
            rep_password: ['', common_1.Validators.compose([common_1.Validators.required, common_1.Validators.minLength(7)])]
        });
        this.socketService.addListener('user-password-updated', function (data) {
            _this.zone.run(function () {
                if (data.err) {
                    _this.submitted = false;
                    _this.error = {
                        type: 'danger',
                        msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                    };
                }
                else {
                    _this.user = data.user;
                    _this.successful = true;
                }
            });
        });
        this.socketService.addListener('recovery-processed', function (data) {
            _this.zone.run(function () {
                if (data.err || !data.user) {
                    _this.error = {
                        type: 'danger',
                        msg: 'No se ha podido recuperar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                    };
                }
            });
        });
        this.socketService.addListener('session', function (data) {
            if (data.err) {
            }
            else if (data.user) {
                document.location.href = '/';
            }
            else {
                _this.socketService.send('process-recovery', {
                    token: _this.params.get('token')
                });
            }
        });
        this.socketService.send('request-session', { session_token: this.socketService.session_token, page: location.pathname });
    };
    RecComponent = __decorate([
        core_1.Component({
            selector: 'amgs-rec',
            templateUrl: 'app/rec/rec.template.html',
            styleUrls: ['app/rec/rec.style.css'],
            providers: [],
            directives: [common_1.FORM_DIRECTIVES]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, (typeof (_a = typeof router_deprecated_1.RouteParams !== 'undefined' && router_deprecated_1.RouteParams) === 'function' && _a) || Object, common_1.FormBuilder])
    ], RecComponent);
    return RecComponent;
    var _a;
}());
exports.RecComponent = RecComponent;
//# sourceMappingURL=rec.component.js.map