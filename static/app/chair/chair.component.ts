import { Component, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';

//import { SocketService } from '../sockets/socket.service';
import { SocketService } from '../sockets/mock-socket.service';
import { NavComponent } from '../nav/nav.component';
import { UsersService } from '../users/user.service';
import { MenuItem } from '../nav/menu-item.model';

@Component({
    selector: 'animagens-chair',
    templateUrl: '/app/chair/chair.template.html',
    styleUrls: ['app/chair/chair.style.css'],
    directives: [NavComponent],
    providers: [],
    animations: [
        trigger('postState', [
            state('void', style({opacity: 0, transform: 'translateY(+200px)'})),
            state('in', style({opacity: 1, transform: 'translateY(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1,
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ]),
        trigger('backgroundState', [
            state('void', style({opacity: 0})),
            state('in', style({opacity: 1})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1
                    }))
                ])
            ])
        ])
    ]
})
export class ChairComponent {
}