import { Component } from '@angular/core';
import { NavComponent } from './nav/nav.component';
import { MenuItem } from './nav/menu-item.model';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import {SocketService} from './sockets/socket.service';

@Component({
    selector: 'animagens-app',
    template: `
        <span defaultOverlayTarget></span>
        <animagens-nav [user]="user" [pageColor]="pageColor" [menuItems]="menuItems"></animagens-nav>
        <router-outlet></router-outlet>
    `,
    directives: [NavComponent],
    providers: []
})
export class AppComponent {
    public user: any;
    public pageColor = 'blue';
    public menuItems: MenuItem[] = [
        new MenuItem('Inicio', '/inicio', 'home', 'blue', true),
        /*new MenuItem('Foro', '/foro', 'beer', 'cyan', false),
        new MenuItem('El Sillón', '/sillon', 'users', 'green', false),
        new MenuItem('De Incógnito', '/incognito', 'user-secret', 'grass', false),
        new MenuItem('Tienda', '/tienda', 'shopping-cart', 'gold', false)*/
    ];

    public windowFocused = true;

    constructor(private socketService: SocketService, router:Router) {
        window.onfocus = (event) => {
            this.windowFocused = true;
        };
        window.onblur = (event) => {
            this.windowFocused = false;
        };
        router.events.subscribe((event:any) => {
            if (event instanceof NavigationStart) {
                this.socketService.clearListeners();
            }
            if(event instanceof NavigationEnd) {
                this.socketService.socket.emit('page-changed', {page: router.url});
                socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});
                socketService.socket.on('session', (data:any) => {
                    if (data.user) {
                        this.user = data.user;
                    } else {
                        this.user = undefined;
                    }
                });
                socketService.socket.on('logout', (data:any) => {
                    router.navigate(['/']);
                    socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});
                });
            }
        });
    }
}