import { Component, Input, Output, EventEmitter } from '@angular/core';

import {UsersService} from '../../users/user.service';

@Component({
    selector: 'amgs-chat-list',
    templateUrl: 'app/chat/list/list.template.html',
    styleUrls: ['app/chat/list/list.style.css'],
    providers: []
})
export class ChatListComponent {
    users = this.usersService.users;

    @Input()
    myUser: any;
    @Input()
    conversations: any[];
    @Input()
    chatUsers: any;
    @Input()
    chatUsersList: any[];
    @Input()
    starreds: any;
    @Input()
    rooms: any;

    @Output()
    newConversation = new EventEmitter();
    @Output()
    selectConversation = new EventEmitter();
    @Output()
    createRoom = new EventEmitter();
    @Output()
    createUserConversation = new EventEmitter();

    openLists = [true, false, false, true];

    selectConversationIndex(index: any) {
        this.selectConversation.next(index);
    }

    selectUser(user_id: any) {
        this.newConversation.next({type: 'user', user_id: user_id});
    };

    constructor(private usersService: UsersService) {
    }
}