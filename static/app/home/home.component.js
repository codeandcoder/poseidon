"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var socket_service_1 = require('../sockets/socket.service');
var nav_component_1 = require('../nav/nav.component');
var user_service_1 = require('../users/user.service');
var HomeComponent = (function () {
    function HomeComponent(socketService, usersService) {
        var _this = this;
        this.socketService = socketService;
        this.usersService = usersService;
        this.users = this.usersService.users;
        socketService.send('request-blogs', {}, function (data) {
            _this.blogs = data.blogs;
            for (var i = 0; i < _this.blogs.length; i++) {
                if (!usersService.users[_this.blogs[i].user_id]) {
                    usersService.getUser(_this.blogs[i].user_id, function () { });
                }
            }
        });
        socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
        socketService.socket.on('session', function (data) {
            if (data.user) {
                _this.user = data.user;
            }
            else {
                _this.user = undefined;
            }
        });
    }
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'animagens-home',
            templateUrl: '/app/home/home.template.html',
            styleUrls: ['app/home/home.style.css'],
            directives: [nav_component_1.NavComponent],
            providers: [],
            animations: [
                core_1.trigger('postState', [
                    core_1.state('void', core_1.style({ opacity: 0, transform: 'translateY(+200px)' })),
                    core_1.state('in', core_1.style({ opacity: 1, transform: 'translateY(0px)' })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1,
                                transform: 'translateY(0px)'
                            }))
                        ])
                    ])
                ]),
                core_1.trigger('backgroundState', [
                    core_1.state('void', core_1.style({ opacity: 0 })),
                    core_1.state('in', core_1.style({ opacity: 1 })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1
                            }))
                        ])
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, user_service_1.UsersService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map