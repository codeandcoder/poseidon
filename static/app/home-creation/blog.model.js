"use strict";
var Blog = (function () {
    function Blog(image, title, content) {
        this.image = image;
        this.title = title;
        this.content = content;
    }
    return Blog;
}());
exports.Blog = Blog;
//# sourceMappingURL=blog.model.js.map