"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var router_1 = require('@angular/router');
var ng2_uploader_1 = require('ng2-uploader/ng2-uploader');
var ng2_ckeditor_1 = require('ng2-ckeditor');
var socket_service_1 = require('../sockets/socket.service');
var nav_component_1 = require('../nav/nav.component');
var HomeEditComponent = (function () {
    function HomeEditComponent(socketService, zone, route) {
        var _this = this;
        this.socketService = socketService;
        this.zone = zone;
        this.route = route;
        this.model = {};
        // Image Upload
        this.imagesUrl = config.imagesUrl;
        this.options = {
            url: config.imagesUploadUrl
        };
        // Form
        this.submitted = false;
        this.route.params.map(function (params) { return params['id']; }).subscribe(function (id) {
            socketService.send('request-blog', { blog_id: id }, function (data) {
                _this.model = data.blog;
            });
        });
    }
    HomeEditComponent.prototype.handleUpload = function (data) {
        if (data && data.response) {
            this.model.image = 'images/users/' + data.response;
        }
    };
    HomeEditComponent.prototype.onSubmit = function () {
        this.submitted = true;
        this.socketService.send('edit-blog', this.model, function (data) {
            window.location.href = '/';
        });
    };
    HomeEditComponent = __decorate([
        core_1.Component({
            selector: 'animagens-home-edit',
            templateUrl: '/app/home-edit/home-edit.template.html',
            styleUrls: ['app/home-edit/home-edit.style.css'],
            directives: [nav_component_1.NavComponent, ng2_uploader_1.UPLOAD_DIRECTIVES, ng2_ckeditor_1.CKEditor],
            providers: [],
            animations: [
                core_1.trigger('postState', [
                    core_1.state('void', core_1.style({ opacity: 0, transform: 'translateY(+200px)' })),
                    core_1.state('in', core_1.style({ opacity: 1, transform: 'translateY(0px)' })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1,
                                transform: 'translateY(0px)'
                            }))
                        ])
                    ])
                ]),
                core_1.trigger('backgroundState', [
                    core_1.state('void', core_1.style({ opacity: 0 })),
                    core_1.state('in', core_1.style({ opacity: 1 })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1
                            }))
                        ])
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, core_2.NgZone, router_1.ActivatedRoute])
    ], HomeEditComponent);
    return HomeEditComponent;
}());
exports.HomeEditComponent = HomeEditComponent;
//# sourceMappingURL=home-edit.component.js.map