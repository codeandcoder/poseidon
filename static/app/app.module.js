"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var app_component_1 = require('./app.component');
var socket_service_1 = require('./sockets/socket.service');
var user_service_1 = require('./users/user.service');
var app_routing_1 = require('./app.routing');
var angular2_modal_1 = require('angular2-modal');
var index_1 = require('angular2-modal/plugins/bootstrap/index');
var status_change_modal_1 = require('./nav/status-change.modal');
var register_component_1 = require('./register/register.component');
var login_component_1 = require('./login/login.component');
var home_component_1 = require('./home/home.component');
var home_creation_component_1 = require('./home-creation/home-creation.component');
var home_edit_component_1 = require('./home-edit/home-edit.component');
var forum_component_1 = require('./forum/forum.component');
var chair_component_1 = require('./chair/chair.component');
var unknown_component_1 = require('./unknown/unknown.component');
var shop_component_1 = require('./shop/shop.component');
var verification_component_1 = require('./verification/verification.component');
var chat_component_1 = require('./chat/chat.component');
var terms_component_1 = require('./terms/terms.component');
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_1.routing, index_1.BootstrapModalModule, angular2_modal_1.ModalModule.forRoot()],
            providers: [socket_service_1.SocketService, user_service_1.UsersService, app_routing_1.appRoutingProviders, platform_browser_1.Title, index_1.Modal],
            declarations: [app_component_1.AppComponent, register_component_1.RegisterComponent, login_component_1.LoginComponent, home_component_1.HomeComponent, home_creation_component_1.HomeCreationComponent, forum_component_1.ForumComponent, chair_component_1.ChairComponent, unknown_component_1.UnknownComponent, shop_component_1.ShopComponent, verification_component_1.VerificationComponent, home_edit_component_1.HomeEditComponent, chat_component_1.ChatComponent, terms_component_1.TermsComponent],
            exports: [app_component_1.AppComponent],
            bootstrap: [app_component_1.AppComponent],
            entryComponents: [status_change_modal_1.StatusChangeModal]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map