import { NgModule }      from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import {AppComponent} from './app.component';

import {SocketService} from './sockets/socket.service';
import {UsersService} from './users/user.service';
import { routing, appRoutingProviders } from './app.routing';
import { ModalModule } from 'angular2-modal';
import { Overlay, overlayConfigFactory, OverlayRenderer} from 'angular2-modal';
import { Modal, BSModalContext, BootstrapModalModule } from 'angular2-modal/plugins/bootstrap/index';

import { CustomModalContext, StatusChangeModal } from './nav/status-change.modal';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { HomeCreationComponent } from './home-creation/home-creation.component';
import { HomeEditComponent } from './home-edit/home-edit.component';
import { ForumComponent } from './forum/forum.component';
import { ChairComponent } from './chair/chair.component';
import { UnknownComponent } from './unknown/unknown.component';
import { ShopComponent } from './shop/shop.component';
import { VerificationComponent } from './verification/verification.component';
import { ChatComponent } from './chat/chat.component';
import { TermsComponent } from './terms/terms.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, routing, BootstrapModalModule, ModalModule.forRoot()],
  providers:    [ SocketService, UsersService, appRoutingProviders, Title, Modal ],
  declarations: [ AppComponent, RegisterComponent, LoginComponent, HomeComponent, HomeCreationComponent, ForumComponent, ChairComponent, UnknownComponent, ShopComponent, VerificationComponent, HomeEditComponent, ChatComponent, TermsComponent ],
  exports:      [ AppComponent ],
  bootstrap:    [ AppComponent ],
  entryComponents: [ StatusChangeModal ]
})
export class AppModule { }