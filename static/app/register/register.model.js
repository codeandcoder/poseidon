"use strict";
var Register = (function () {
    function Register(nickname, alias, email, rep_email, password, rep_password, birth, gender) {
        this.nickname = nickname;
        this.alias = alias;
        this.email = email;
        this.rep_email = rep_email;
        this.password = password;
        this.rep_password = rep_password;
        this.birth = birth;
        this.gender = gender;
    }
    return Register;
}());
exports.Register = Register;
//# sourceMappingURL=register.model.js.map