"use strict";
var MenuItem = (function () {
    function MenuItem(title, link, icon, color, active) {
        this.title = title;
        this.link = link;
        this.icon = icon;
        this.color = color;
        this.active = active;
    }
    return MenuItem;
}());
exports.MenuItem = MenuItem;
//# sourceMappingURL=menu-item.model.js.map