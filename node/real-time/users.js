var db = require('../db');
var logger = require('../logger');
var utils = require('../utils');
var constants = require('../constants');
var enotifier = require('../enotifier');

exports.config = function(socket) {
    socket.on('request-user', function(request) {
        var data = request.data;
        if (data.user_id) {
            db.findUserById(parseInt(data.user_id), function(err, user) {
                if(err) {
                    logger.error(err);
                } else if (!user) {
                    // Do nothing
                } else if (!socket.user || user._id != socket.user._id) {
                    socket.emit('response', {id: request.id, data: user});
                } else {
                    db.findUserInfo(user._id, function(err, userInfo) {
                        if (userInfo) {
                            user.email = userInfo.email;
                            user.birth = userInfo.birth;
                            user.gender = userInfo.gender;
                        }
                        db.findUserConfig(user._id, function(err, config) {
                            if (config) {
                                user.sound = config.sound;
                            }
                            db.findUserPerms(user._id, function(err, perms) {
                                user.perms = perms;
                                socket.emit('response', {id: request.id, data: user});
                            });
                        });
                    });
                }
            });
        } else if (data.nickname) {
            db.findUserByNickname(data.nickname, function(err, user) {
                if(err) {
                    logger.error(err);
                } else if (!user) {
                    // Do nothing
                } else if (user._id != socket.user._id && user.confidential) {
                    socket.emit('response', {id: request.id, data: user});
                } else {
                    db.findUserInfo(user._id, function(err, userInfo) {
                        if (userInfo) {
                            user.email = userInfo.email;
                            user.birth = userInfo.birth;
                            user.gender = userInfo.gender;
                        }
                        db.findUserConfig(user._id, function(err, config) {
                            if (config) {
                                user.sound = config.sound;
                            }
                            db.findUserPerms(user._id, function(err, perms) {
                                user.perms = perms;
                                socket.emit('response', {id: request.id, data: user});
                            });
                        });
                    });
                }
            });
        }
    });

    socket.on('request-recovery', function(request) {
        var data = request.data;
        var token = utils.genToken();
        db.findUserByEmail(data.email, function(err, user) {
            if (err) return socket.emit('response', {id: request.id, data: {err:err}});
            
            if (user) {
                db.createRecovery(data.email, token, function(err) {
                    enotifier.sendMail({
                        mails: [data.email],
                        subject: 'Recuperación de la cuenta',
                        content: "<p>Hemos recibido una solicitud para recuperar los datos de tu cuenta.</p>"
                                    + "<p>Si no has sido tú el solicitante, puedes contactar con nosotros en soporte@animagens.es; aunque tus datos siguen siendo completamente seguros.</p>"
                                    + "<p>Si, por lo contrario, quieres recuperar tu cuenta, accede al siguiente enlace: <a href=\"https://www.animagens.es/rec?token=" + token + "\">Recuperar Cuenta</a></p>"
                                    + "<br/><p>¡Esperamos verte pronto!</p>"
                                    + "<p>El equipo de Animagens</p>"
                    }, function(err) {
                        return socket.emit('response', {id: request.id, data: {err:err}});
                    })
                });
            } else {
                return socket.emit('response', {id: request.id, data: {err:constants.getError('c117')}});
            }
        });
    });

    socket.on('process-recovery', function(request) {
        var data = request.data;
        db.findRecovery(data.token, function(err, user) {
            if (err) return socket.emit('response', {íd: request.id, data:{err: err}});
            return socket.emit('response', {íd: request.id, data:{user: user}});
        });
    });

    socket.on('update-rec-user-password', function(request) {
        var data = request.data;
        db.findRecovery(data.token, function(err, user) {
            if (err) return socket.emit('response', {id: request.id, data:{err: err}});
            if (user) {
                db.findUserAuthorization(user.nickname, function(err, auth) {
                    if (err) return socket.emit('response', {id: request.id, data:{err: err}});
                    if (!auth)  {
                        return socket.emit('response', {id: request.id, data:{err: constants.getError('c999')}});
                    }
                    utils.hashPass(data.password, auth.salt, function(err, hashedPassword) {
                        db.updateUserPassword(user._id, hashedPassword, function(err) {
                            if (err) return socket.emit('response', {id: request.id, data:{err: err}});
                            
                            db.removeRecovery(data.token, function(err) {
                                return socket.emit('response', {id: request.id, data:{err: err, user: user}});
                            });
                        });
                    });
                });
            } else {
                return socket.emit('response', {id: request.id, data:{err: constants.getError('c999')}});
            }
        });
    });

    socket.on('update-user', function(request) {
        var user = request.data;
        if (socket.user && socket.user._id == user._id) {
            user.config = {
                sound: user.sound
            };
            db.saveUser(user, function(err) {
                return socket.emit('response', {id: request.id, data:{err: err}});
            });
        } else {
            return socket.emit('response', {id: request.id, data:{err: 'No Perms'}});
        }
    });

    socket.on('request-login', function(request) {
        var data = request.data;
        var ip = socket.handshake.headers['x-forwarded-for'] || socket.request.connection.remoteAddress;
        if (data.nickname.indexOf('@') > -1) {
            db.findUserByEmail(data.nickname, function(err, user) {
                if (err) return socket.emit('response', {id: request.id, data:{err: err}});

                if (user) {
                    processLogin(data, user, ip, function (err, session, user) {
                        if (err) return socket.emit('response', {id: request.id, data:{err: err}});
                        return socket.emit('response', {id: request.id, data:{session_token: session.token, user: user}});
                    });
                } else {
                    return socket.emit('response', {id: request.id, data:{err: constants.getError('c117')}});
                }
            });
        } else {
            data.nickname = data.nickname.toLowerCase();
            db.findUserByNickname(data.nickname, function(err, user) {
                if (err) return socket.emit('response', {id: request.id, data:{err: err}});
                if (user) {
                    processLogin(data, user, ip, function (err, session, user) {
                        if (err) return socket.emit('response', {id: request.id, data:{err: err}});
                        return socket.emit('response', {id: request.id, data:{session_token: session.token, user: user}});
                    });
                } else {
                    return socket.emit('response', {id: request.id, data:{err: constants.getError('c117')}});
                }
            });
        }
    });

    socket.on('register', function(request) {
        var data = request.data;
        data.nickname = data.nickname.toLowerCase();
        db.findUserByNickname(data.nickname, function(err, user) {
            if (user) {
                return socket.emit('response', {id: request.id, data:{err: constants.getError('c101')}});
            }

            db.findUserByEmail(data.email, function(err, userInfo) {
                if (userInfo) {
                    return socket.emit('response', {id: request.id, data:{err: constants.getError('c102')}});
                }

                var salt = utils.genToken();
                var verification_token = utils.genToken();
                utils.hashPass(data.password, salt, function(err, hashedPassword) {
                    var user = {
                        nickname: data.nickname,
                        alias: data.alias,
                        gender: data.gender,
                        password: hashedPassword,
                        salt: salt,
                        birth: data.birth,
                        email: data.email,
                        verification_token: verification_token
                    };
                    db.saveUser(user, function(err) {
                        if (err) return socket.emit('response', {id: request.id, data:{err: err}});

                        enotifier.sendMail({
                            mails: [data.email],
                            subject: '¡Bienvenido/a a Animagens!',
                            content: "<p>De parte del equipo de Animagens, ¡bienvenido/a a nuestra comunidad!</p>"
                            + "<p>Con tal de confirmar tu registro de forma definitiva, debes acceder al siguiente enlace:</p>"
                            + "<p><a href=\"https://www.animagens.es/verificar;token=" + verification_token + "\">Confirmar registro</a></p>"
                            + "<br/><p>¡Esperamos verte pronto!</p>"
                            + "<p>El equipo de Animagens</p>"
                        }, function(err) {
                            return socket.emit('response', {id: request.id, data:{err: err}});
                        });
                    });
                });
            });
        });
    });

    socket.on('request-verification', function(request) {
        var data = request.data;
        db.verifyUser(data.token, function(err) {
            return socket.emit('response', {id: request.id, data:{err: err}});
        });
    });
};

function processLogin(data, user, ip, callback) {
    db.findUserVerification(user.nickname, function(err, verification) {
        if (err) return callback(err);

        if (verification) {
            db.findUserAuthorization(user.nickname, function(err, authorization) {
                if (authorization) {
                    utils.hashPass(data.password, authorization.salt, function(err, hashedPassword) {
                        if (err) return callback(err);

                        if (hashedPassword == authorization.password) {
                            db.findUserInfo(user._id, function(err, userInfo) {
                                if (userInfo) {
                                    user.email = userInfo.email;
                                    user.birth = userInfo.birth;
                                    user.gender = userInfo.gender;
                                }
                                db.findUserConfig(user._id, function(err, config) {
                                    if (config) {
                                        user.sound = config.sound;
                                    }
                                    var session = {
                                        token: utils.genToken(),
                                        user_id: user._id,
                                        ip: ip,
                                        timestamp: new Date().getTime()
                                    };
                                    db.createSession(session, function(err) {
                                        return callback(null, session, user);
                                    });
                                });
                            });
                        } else {
                            // Credentials error
                            return callback(constants.getError('c109'));
                        }
                    });
                } else {
                    var error = constants.getError('c999');
                    return callback(error);
                }
            });
        } else {
            var error = constants.getError('c110');
            return callback(error);
        }
    });
}