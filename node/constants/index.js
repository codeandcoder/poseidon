exports.defaultUserTextColor = '#000000';
exports.defaultUserProfileImage = 'https://jamfnation.jamfsoftware.com/img/default-avatars/generic-user.png';
exports.defaultUserPrivate = false;

var errors = {
		c100: {
			code: 100,
			msg: 'Not yet implemented!'
		},
		c101: {
			code: 101,
			msg: 'Nickname already taken'
		},
		c102: {
			code: 102,
			msg: 'Email already taken'
		},
		c103: {
			code: 103,
			msg: 'Expired token or non existent'
		},
		c104: {
			code: 104,
			msg: 'Captcha failed'
		},
		c105: {
			code: 105,
			msg: 'Missing field'
		},
		c106: {
			code: 106,
			msg: 'Unknown operation'
		},
		c107: {
			code: 107,
			msg: 'Confirmation token not valid'
		},
		c108: {
			code: 108,
			msg: 'Programmer error'
		},
		c109: {
			code: 109,
			msg: 'Wrong credentials'
		},
		c110: {
			code: 110,
			msg: 'User not verified'
		},
		c111: {
			code: 111,
			msg: 'Already loged in'
		},
		c112: {
			code: 112,
			msg: 'Not enough permissions'
		},
		c113: {
			code: 113,
			msg: 'Incorrect password format'
		},
		c114: {
			code: 114,
			msg: 'User is banned'
		},
		c115: {
			code: 115,
			msg: 'Last comment is from the same user'
		},
		c116: {
			code: 116,
			msg: 'Reached post limit'
		},
		c117: {
			code: 117,
			msg: 'User does not exist (or is removed)'
		},
		c999: {
			code: 999,
			msg: 'Unknown error'
		}
};

exports.getError = function(code) {
	if (errors[code] !== undefined) {
		return JSON.parse(JSON.stringify(errors[code]));
	}
	
	return {
		code: 1000,
		msg: 'Unexpected Error'
	};
};