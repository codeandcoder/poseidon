"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var user_service_1 = require('../../users/user.service');
var ChatListComponent = (function () {
    function ChatListComponent(usersService) {
        this.usersService = usersService;
        this.users = this.usersService.users;
        this.newConversation = new core_1.EventEmitter();
        this.selectConversation = new core_1.EventEmitter();
        this.createRoom = new core_1.EventEmitter();
        this.createUserConversation = new core_1.EventEmitter();
        this.openLists = [true, false, false, true];
    }
    ChatListComponent.prototype.selectConversationIndex = function (index) {
        this.selectConversation.next(index);
    };
    ChatListComponent.prototype.selectUser = function (user_id) {
        this.newConversation.next({ type: 'user', user_id: user_id });
    };
    ;
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "myUser", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ChatListComponent.prototype, "conversations", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "chatUsers", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], ChatListComponent.prototype, "chatUsersList", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "starreds", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "rooms", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "newConversation", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "selectConversation", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "createRoom", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], ChatListComponent.prototype, "createUserConversation", void 0);
    ChatListComponent = __decorate([
        core_1.Component({
            selector: 'amgs-chat-list',
            templateUrl: 'app/chat/list/list.template.html',
            styleUrls: ['app/chat/list/list.style.css'],
            providers: []
        }), 
        __metadata('design:paramtypes', [user_service_1.UsersService])
    ], ChatListComponent);
    return ChatListComponent;
}());
exports.ChatListComponent = ChatListComponent;
//# sourceMappingURL=list.component.js.map