import { Host, Component } from '@angular/core';
import { NgZone } from '@angular/core';
import {Angular2AutoScroll} from "angular2-auto-scroll/lib/angular2-auto-scroll.directive";
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { MenuItem } from '../nav/menu-item.model';

import { ActiveConversationComponent } from './conversation/active-conversation.component';

import { AppComponent } from '../app.component';
import { SocketService } from '../sockets/socket.service';
import { ChatListComponent } from './list/list.component';
import {UsersService} from '../users/user.service';

@Component({
    selector: 'animagens-chat',
    templateUrl: '/app/chat/chat.template.html',
    styleUrls: ['app/chat/chat.style.css'],
    directives: [ActiveConversationComponent, ChatListComponent, Angular2AutoScroll]
})
export class ChatComponent {
    user:any;
    users = this.usersService.users;
    activeConversation: any;
    conversations: any[] = [];
    chatUsers: any = {};
    chatUsersList: any[] = [];
    chatRooms: any = [];
    starreds: any[] = [];
    openLists = [true, true, true];
    pendingMessages = 0;

    message: string = '';

    selectConversation(index:any) {
        this.activeConversation = this.conversations[index];
        this.activeConversation.active = false;
        this.pendingMessages = 0;
        this.title.setTitle('Animagens');
    }

    newConversation(data:any, user_opened: boolean) {
        if (data.type == 'user') {
            if (!this.users[data.user_id]) {
                this.usersService.getUser(data.user_id, (u:any) => {});
            }
            var index = this.findConversationIndex({type: 'user', data: {_id: parseInt(data.user_id)}});
            if (index < 0) {
                this.conversations.push({type: 'user', data: {_id: parseInt(data.user_id)}, messages: []});
                if (user_opened) {
                    this.activeConversation = this.conversations[this.conversations.length-1];
                } else {
                    this.conversations[this.conversations.length-1].active = true;
                }
                this.socketService.send('request-history', {sender_id: this.user._id, target_id: parseInt(data.user_id), from:0, amount:25}, (messages:any) => {
                    var i = this.findConversationIndex({type: 'user', data: {_id: parseInt(data.user_id)}});
                    if (i > -1) {
                        for (let j = 0; j < messages.length; j++) {
                            messages[j].user = this.users[messages[j].sender_id];
                        }
                        this.conversations[i].messages.splice(0,this.conversations[i].messages.length);
                        for (let k = 0; k < messages.length; k++) {
                            if (this.conversations[i].messages.length > 0
                                && this.conversations[i].messages[this.conversations[i].messages.length-1].sender_id == messages[k].sender_id) {
                                    this.conversations[i].messages[this.conversations[i].messages.length-1].created_at += messages[k].created_at;
                                    this.conversations[i].messages[this.conversations[i].messages.length-1].content += messages[k].content;
                            } else {
                                this.conversations[i].messages.push(messages[k]);
                            }
                        }
                    }
                });
            } else {
                if (user_opened) {
                    this.activeConversation = this.conversations[index];
                } else {
                    this.conversations[index].active = true;
                }
            }
        } else {
            var room = this.findRoom(data.room_id);
            if (!this.users[room.user_id]) {
                this.usersService.getUser(room.user_id, (u:any) => {});
            }
            var index = this.findConversationIndex({type: 'room', data: {_id: parseInt(room._id)}});
            if (index < 0) {
                this.conversations.push({type: 'room', data: room, messages: []});
                if (user_opened) {
                    this.activeConversation = this.conversations[this.conversations.length-1];
                } else {
                    this.conversations[this.conversations.length-1].active = true;
                }
                this.socketService.send('request-history', {sender_id: this.user._id, target_id: parseInt(room._id), from:0, amount:25}, (messages:any) => {
                    var i = this.findConversationIndex({type: 'room', data: {_id: parseInt(room._id)}});
                    if (i > -1) {
                        for (let j = 0; j < messages.length; j++) {
                            this.usersService.getUser(messages[j].sender_id, (u:any) => {
                                messages[j].user = u;
                            });
                        }
                        this.conversations[i].messages.splice(0,this.conversations[i].messages.length);
                        for (let k = 0; k < messages.length; k++) {
                            if (this.conversations[i].messages.length > 0
                                && this.conversations[i].messages[this.conversations[i].messages.length-1].sender_id == messages[k].sender_id) {
                                    this.conversations[i].messages[this.conversations[i].messages.length-1].created_at += messages[k].created_at;
                                    this.conversations[i].messages[this.conversations[i].messages.length-1].content += messages[k].content;
                            } else {
                                this.conversations[i].messages.push(messages[k]);
                            }
                        }
                    }
                });
            } else {
                if (user_opened) {
                    this.activeConversation = this.conversations[index];
                } else {
                    this.conversations[index].active = true;
                }
            }
        }
    }

    closeConversation() {
        for (let i = 0; i < this.conversations.length; i++) {
            if (this.conversations[i].data._id == this.activeConversation.data._id) {
                this.conversations.splice(i,1);
                this.activeConversation = undefined;
                break;
            }
        }
    }
    
    createRoom(data:any) {

    }

    createUserConversation(data:any) {

    }

    makeStarred() {
        if (this.starreds.indexOf(this.activeConversation.data._id) < 0) {
            this.socketService.socket.emit('make-starred', {user_id: this.activeConversation.data._id});
        } else {
            this.socketService.socket.emit('make-unstarred', {user_id: this.activeConversation.data._id});
        }
    }

    onSubmit() {
        if (this.message.trim().length > 0) {
            this.socketService.socket.emit('message', {message: this.message.trim(), type: this.activeConversation.type, _id: this.activeConversation.data._id});
            this.message = '';
        }
    }

    findConversationIndex(params:any): number {
        for (let i = 0; i < this.conversations.length; i++) {
            var c = this.conversations[i];
            if (c.type == params.type && c.data._id == params.data._id) {
                return i;
            }
        }
        return -1;
    }

    findRoom(room_id:any) {
        for (let i = 0; i < this.chatRooms.length; i++) {
            if (this.chatRooms[i]._id == room_id) {
                return this.chatRooms[i];
            }
        }
        return null;
    }

    constructor(private usersService: UsersService, private socketService: SocketService, private zone: NgZone, private router: Router, private title: Title, @Host() appComponent: AppComponent) {
        socketService.socket.on('message', (data: any) => {
            let user_id = data.sender_id == this.user._id ? data.target_id : data.sender_id;
            let found = false;
            for (let i = 0; i < this.conversations.length && !found; i++) {
                if (this.conversations[i].data._id == user_id) {
                    data.user = this.users[data.sender_id];
                    if (this.conversations[i].messages.length > 0 && this.conversations[i].messages[this.conversations[i].messages.length-1].sender_id == data.sender_id) {
                        this.conversations[i].messages[this.conversations[i].messages.length-1].created_at = data.created_at;
                        this.conversations[i].messages[this.conversations[i].messages.length-1].content += data.content;
                    } else {
                        this.conversations[i].messages.push(data);
                    }
                    if (!this.activeConversation || !appComponent.windowFocused || this.conversations[i].data._id != this.activeConversation.data._id) {
                        this.pendingMessages++;
                        this.title.setTitle('(' + this.pendingMessages + ') Animagens');
                    }
                    found = true;
                }
            }
            if (!found) {
                this.newConversation({
                    type: data.type,
                    user_id: data.sender_id,
                    room_id: data.target_id
                }, false);
                this.pendingMessages++;
                this.title.setTitle('(' + this.pendingMessages + ') Animagens');
            }
        });
        socketService.socket.on('starreds', (data: any) => {
            if (data.err) {
                // do nothing
            } else {
                this.starreds = data.starreds;
                let downloadCount = 0;
                for (let i = 0; i < data.starreds.length; i++) {
                    this.usersService.getUser(data.starreds[i], (u:any) => {
                        downloadCount++;
                        if (downloadCount == this.starreds.length) {
                            this.starreds.sort((a:any,b:any) => {
                                if(this.users[a].alias < this.users[b].alias) return -1;
                                if(this.users[a].alias > this.users[b].alias) return 1;
                                return 0;
                            });
                        }
                    });
                }
            }
        });
        socketService.socket.on('chat-users', (data: any) => {
            if (data.err) {
                this.chatUsers = {};
                this.chatUsersList = [];
            } else {
                if (this.user) {
                    delete data.chatUsers[this.user._id];
                }
                this.chatUsers = data.chatUsers;
                this.chatUsersList = Object.keys(this.chatUsers);
                let downloadCount = 0;
                for (var i = 0; i < this.chatUsersList.length; i++) {
                    this.usersService.getUser(this.chatUsersList[i], (u:any) => {
                        downloadCount++;
                        if (downloadCount == this.chatUsersList.length) {
                            this.chatUsersList.sort((a:any,b:any) => {
                                if(this.users[a].alias < this.users[b].alias) return -1;
                                if(this.users[a].alias > this.users[b].alias) return 1;
                                return 0;
                            });
                        }
                    });
                }
            }
        });
        /*socketService.socket.on('chat-rooms', (data: any) => {
            if (data.err) {
                this.chatRooms = [];
            } else {
                this.chatRooms = data.chatRooms;
                for (let i = 0; i < this.chatRooms.length; i++) {
                    this.chatRooms[i].users = [];
                }
                this.chatRooms.sort((a:any,b:any) => {
                    if(a.name < b.name) return -1;
                    if(a.name > b.name) return 1;
                    return 0;
                });
            }
        });*/
        socketService.socket.on('session', (data: any) => {
            if (data.user) {
                this.user = data.user;
                if (this.chatUsersList.length > 0) {
                    delete this.chatUsers[this.user._id];
                    this.chatUsersList.splice(this.chatUsersList.indexOf(this.user._id),1);
                }
                socketService.socket.emit('request-chat-users', {});
                socketService.socket.emit('request-starreds', {});
                socketService.send('request-pendings', {}, (conversations:any) => {
                    this.usersService.getUser(this.user._id, (u:any) => {
                        for (let i = 0; i < conversations.length; i++) {
                            this.usersService.getUser(conversations[i].conversation.sender_id, (u:any) => {
                                this.newConversation({type: 'user', user_id: conversations[i].conversation.sender_id}, true);
                            });
                        }
                    });
                });
            } else {
                router.navigate(['/']);
            }
        });
        socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});

        setInterval(() => {
            if (appComponent.windowFocused) {
                this.pendingMessages = 0;   
            }
        }, 500)
    }
}