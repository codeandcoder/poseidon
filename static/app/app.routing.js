"use strict";
var router_1 = require('@angular/router');
var register_component_1 = require('./register/register.component');
var login_component_1 = require('./login/login.component');
var home_component_1 = require('./home/home.component');
var home_creation_component_1 = require('./home-creation/home-creation.component');
var home_edit_component_1 = require('./home-edit/home-edit.component');
var forum_component_1 = require('./forum/forum.component');
var chair_component_1 = require('./chair/chair.component');
var unknown_component_1 = require('./unknown/unknown.component');
var shop_component_1 = require('./shop/shop.component');
var verification_component_1 = require('./verification/verification.component');
var chat_component_1 = require('./chat/chat.component');
var terms_component_1 = require('./terms/terms.component');
var appRoutes = [
    {
        path: 'inicio',
        component: home_component_1.HomeComponent
    },
    {
        path: '',
        component: home_component_1.HomeComponent
    },
    {
        path: 'registro',
        component: register_component_1.RegisterComponent
    },
    {
        path: 'terms',
        component: terms_component_1.TermsComponent
    },
    {
        path: 'verificar',
        component: verification_component_1.VerificationComponent
    },
    {
        path: 'entrar',
        component: login_component_1.LoginComponent
    },
    {
        path: 'inicio-creacion',
        component: home_creation_component_1.HomeCreationComponent
    },
    {
        path: 'inicio-edicion',
        component: home_edit_component_1.HomeEditComponent
    },
    {
        path: 'foro',
        component: forum_component_1.ForumComponent
    },
    {
        path: 'sillon',
        component: chair_component_1.ChairComponent
    },
    {
        path: 'incognito',
        component: unknown_component_1.UnknownComponent
    },
    {
        path: 'tienda',
        component: shop_component_1.ShopComponent
    },
    {
        path: 'chat',
        component: chat_component_1.ChatComponent
    }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map