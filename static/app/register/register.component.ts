import { Component, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';
import {ReCaptchaComponent} from 'angular2-recaptcha/angular2-recaptcha';

import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { UsersService } from '../users/user.service';
import { MenuItem } from '../nav/menu-item.model';
import { Register } from './register.model';

@Component({
    selector: 'animagens-register',
    templateUrl: '/app/register/register.template.html',
    styleUrls: ['app/register/register.style.css'],
    directives: [NavComponent, ReCaptchaComponent],
    providers: [],
    animations: [
        trigger('postState', [
            state('void', style({opacity: 0, transform: 'translateY(+200px)'})),
            state('in', style({opacity: 1, transform: 'translateY(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1,
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ]),
        trigger('backgroundState', [
            state('void', style({opacity: 0})),
            state('in', style({opacity: 1})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1
                    }))
                ])
            ])
        ])
    ]
})
export class RegisterComponent {

    model = new Register('', '', '', '', '', '', '', 'unknown');

    submitted = false;
    successful = false;
    genderOptions: any[] = [
        {value: 'unknown', name: 'Sin especificar'},
        {value: 'male', name: 'Hombre'},
        {value: 'female', name: 'Mujer'}
    ];
    error: any;
    captcha: any;
    captcha_resolved = false;
    handleCorrectCaptcha(event: any) {
         this.captcha_resolved = true;
    }

    onSubmit() {
        this.error = null;

        if (!config.test && !this.captcha_resolved) {
            this.error = {
                type: 'danger',
                msg: 'Captcha erróneo'
            };
        } else if (this.model.password != this.model.rep_password) {
            this.error = {
                type: 'danger',
                msg: 'Las passwords no coinciden'
            };
        } else if (this.model.email != this.model.rep_email) {
            this.error = {
                type: 'danger',
                msg: 'Los correos electrónicos no coinciden'
            };       
        } else {
            this.submitted = true;
            this.socketService.send('register', this.model, (data:any) => {
                this.submitted = false;
                if (!data.err) {
                    this.successful = true;
                } else if (data.err.code == 101) {
                    this.error = {
                        type: 'danger',
                        msg: 'Ese nombre de usuario ya esta en uso'
                    };
                } else if (data.err.code == 102) {
                    this.error = {
                        type: 'danger',
                        msg: 'Esa dirección de correo elctrónico ya esta en uso'
                    };
                } else {
                    this.error = {
                        type: 'danger',
                        msg: 'Error desconocido. Por favor, contacta con nosotros en soporte@animagens.es para arreglarlo lo antes posible'
                    };
                }
            });
        }
       
    }

    constructor(private socketService: SocketService) {
        socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});
        socketService.socket.on('session', (data:any) => {
            if (data.user) {
                window.location.href = '/';
            }
        });
    }
}