"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
var core_2 = require('@angular/core');
var angular2_auto_scroll_directive_1 = require("angular2-auto-scroll/lib/angular2-auto-scroll.directive");
var router_1 = require('@angular/router');
var platform_browser_1 = require('@angular/platform-browser');
var active_conversation_component_1 = require('./conversation/active-conversation.component');
var app_component_1 = require('../app.component');
var socket_service_1 = require('../sockets/socket.service');
var list_component_1 = require('./list/list.component');
var user_service_1 = require('../users/user.service');
var ChatComponent = (function () {
    function ChatComponent(usersService, socketService, zone, router, title, appComponent) {
        var _this = this;
        this.usersService = usersService;
        this.socketService = socketService;
        this.zone = zone;
        this.router = router;
        this.title = title;
        this.users = this.usersService.users;
        this.conversations = [];
        this.chatUsers = {};
        this.chatUsersList = [];
        this.chatRooms = [];
        this.starreds = [];
        this.openLists = [true, true, true];
        this.pendingMessages = 0;
        this.message = '';
        socketService.socket.on('message', function (data) {
            var user_id = data.sender_id == _this.user._id ? data.target_id : data.sender_id;
            var found = false;
            for (var i = 0; i < _this.conversations.length && !found; i++) {
                if (_this.conversations[i].data._id == user_id) {
                    data.user = _this.users[data.sender_id];
                    if (_this.conversations[i].messages.length > 0 && _this.conversations[i].messages[_this.conversations[i].messages.length - 1].sender_id == data.sender_id) {
                        _this.conversations[i].messages[_this.conversations[i].messages.length - 1].created_at = data.created_at;
                        _this.conversations[i].messages[_this.conversations[i].messages.length - 1].content += data.content;
                    }
                    else {
                        _this.conversations[i].messages.push(data);
                    }
                    if (!_this.activeConversation || !appComponent.windowFocused || _this.conversations[i].data._id != _this.activeConversation.data._id) {
                        _this.pendingMessages++;
                        _this.title.setTitle('(' + _this.pendingMessages + ') Animagens');
                    }
                    found = true;
                }
            }
            if (!found) {
                _this.newConversation({
                    type: data.type,
                    user_id: data.sender_id,
                    room_id: data.target_id
                }, false);
                _this.pendingMessages++;
                _this.title.setTitle('(' + _this.pendingMessages + ') Animagens');
            }
        });
        socketService.socket.on('starreds', function (data) {
            if (data.err) {
            }
            else {
                _this.starreds = data.starreds;
                var downloadCount_1 = 0;
                for (var i = 0; i < data.starreds.length; i++) {
                    _this.usersService.getUser(data.starreds[i], function (u) {
                        downloadCount_1++;
                        if (downloadCount_1 == _this.starreds.length) {
                            _this.starreds.sort(function (a, b) {
                                if (_this.users[a].alias < _this.users[b].alias)
                                    return -1;
                                if (_this.users[a].alias > _this.users[b].alias)
                                    return 1;
                                return 0;
                            });
                        }
                    });
                }
            }
        });
        socketService.socket.on('chat-users', function (data) {
            if (data.err) {
                _this.chatUsers = {};
                _this.chatUsersList = [];
            }
            else {
                if (_this.user) {
                    delete data.chatUsers[_this.user._id];
                }
                _this.chatUsers = data.chatUsers;
                _this.chatUsersList = Object.keys(_this.chatUsers);
                var downloadCount_2 = 0;
                for (var i = 0; i < _this.chatUsersList.length; i++) {
                    _this.usersService.getUser(_this.chatUsersList[i], function (u) {
                        downloadCount_2++;
                        if (downloadCount_2 == _this.chatUsersList.length) {
                            _this.chatUsersList.sort(function (a, b) {
                                if (_this.users[a].alias < _this.users[b].alias)
                                    return -1;
                                if (_this.users[a].alias > _this.users[b].alias)
                                    return 1;
                                return 0;
                            });
                        }
                    });
                }
            }
        });
        /*socketService.socket.on('chat-rooms', (data: any) => {
            if (data.err) {
                this.chatRooms = [];
            } else {
                this.chatRooms = data.chatRooms;
                for (let i = 0; i < this.chatRooms.length; i++) {
                    this.chatRooms[i].users = [];
                }
                this.chatRooms.sort((a:any,b:any) => {
                    if(a.name < b.name) return -1;
                    if(a.name > b.name) return 1;
                    return 0;
                });
            }
        });*/
        socketService.socket.on('session', function (data) {
            if (data.user) {
                _this.user = data.user;
                if (_this.chatUsersList.length > 0) {
                    delete _this.chatUsers[_this.user._id];
                    _this.chatUsersList.splice(_this.chatUsersList.indexOf(_this.user._id), 1);
                }
                socketService.socket.emit('request-chat-users', {});
                socketService.socket.emit('request-starreds', {});
                socketService.send('request-pendings', {}, function (conversations) {
                    _this.usersService.getUser(_this.user._id, function (u) {
                        var _loop_1 = function(i) {
                            _this.usersService.getUser(conversations[i].conversation.sender_id, function (u) {
                                _this.newConversation({ type: 'user', user_id: conversations[i].conversation.sender_id }, true);
                            });
                        };
                        for (var i = 0; i < conversations.length; i++) {
                            _loop_1(i);
                        }
                    });
                });
            }
            else {
                router.navigate(['/']);
            }
        });
        socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
        setInterval(function () {
            if (appComponent.windowFocused) {
                _this.pendingMessages = 0;
            }
        }, 500);
    }
    ChatComponent.prototype.selectConversation = function (index) {
        this.activeConversation = this.conversations[index];
        this.activeConversation.active = false;
        this.pendingMessages = 0;
        this.title.setTitle('Animagens');
    };
    ChatComponent.prototype.newConversation = function (data, user_opened) {
        var _this = this;
        if (data.type == 'user') {
            if (!this.users[data.user_id]) {
                this.usersService.getUser(data.user_id, function (u) { });
            }
            var index = this.findConversationIndex({ type: 'user', data: { _id: parseInt(data.user_id) } });
            if (index < 0) {
                this.conversations.push({ type: 'user', data: { _id: parseInt(data.user_id) }, messages: [] });
                if (user_opened) {
                    this.activeConversation = this.conversations[this.conversations.length - 1];
                }
                else {
                    this.conversations[this.conversations.length - 1].active = true;
                }
                this.socketService.send('request-history', { sender_id: this.user._id, target_id: parseInt(data.user_id), from: 0, amount: 25 }, function (messages) {
                    var i = _this.findConversationIndex({ type: 'user', data: { _id: parseInt(data.user_id) } });
                    if (i > -1) {
                        for (var j = 0; j < messages.length; j++) {
                            messages[j].user = _this.users[messages[j].sender_id];
                        }
                        _this.conversations[i].messages.splice(0, _this.conversations[i].messages.length);
                        for (var k = 0; k < messages.length; k++) {
                            if (_this.conversations[i].messages.length > 0
                                && _this.conversations[i].messages[_this.conversations[i].messages.length - 1].sender_id == messages[k].sender_id) {
                                _this.conversations[i].messages[_this.conversations[i].messages.length - 1].created_at += messages[k].created_at;
                                _this.conversations[i].messages[_this.conversations[i].messages.length - 1].content += messages[k].content;
                            }
                            else {
                                _this.conversations[i].messages.push(messages[k]);
                            }
                        }
                    }
                });
            }
            else {
                if (user_opened) {
                    this.activeConversation = this.conversations[index];
                }
                else {
                    this.conversations[index].active = true;
                }
            }
        }
        else {
            var room = this.findRoom(data.room_id);
            if (!this.users[room.user_id]) {
                this.usersService.getUser(room.user_id, function (u) { });
            }
            var index = this.findConversationIndex({ type: 'room', data: { _id: parseInt(room._id) } });
            if (index < 0) {
                this.conversations.push({ type: 'room', data: room, messages: [] });
                if (user_opened) {
                    this.activeConversation = this.conversations[this.conversations.length - 1];
                }
                else {
                    this.conversations[this.conversations.length - 1].active = true;
                }
                this.socketService.send('request-history', { sender_id: this.user._id, target_id: parseInt(room._id), from: 0, amount: 25 }, function (messages) {
                    var i = _this.findConversationIndex({ type: 'room', data: { _id: parseInt(room._id) } });
                    if (i > -1) {
                        var _loop_2 = function(j) {
                            _this.usersService.getUser(messages[j].sender_id, function (u) {
                                messages[j].user = u;
                            });
                        };
                        for (var j = 0; j < messages.length; j++) {
                            _loop_2(j);
                        }
                        _this.conversations[i].messages.splice(0, _this.conversations[i].messages.length);
                        for (var k = 0; k < messages.length; k++) {
                            if (_this.conversations[i].messages.length > 0
                                && _this.conversations[i].messages[_this.conversations[i].messages.length - 1].sender_id == messages[k].sender_id) {
                                _this.conversations[i].messages[_this.conversations[i].messages.length - 1].created_at += messages[k].created_at;
                                _this.conversations[i].messages[_this.conversations[i].messages.length - 1].content += messages[k].content;
                            }
                            else {
                                _this.conversations[i].messages.push(messages[k]);
                            }
                        }
                    }
                });
            }
            else {
                if (user_opened) {
                    this.activeConversation = this.conversations[index];
                }
                else {
                    this.conversations[index].active = true;
                }
            }
        }
    };
    ChatComponent.prototype.closeConversation = function () {
        for (var i = 0; i < this.conversations.length; i++) {
            if (this.conversations[i].data._id == this.activeConversation.data._id) {
                this.conversations.splice(i, 1);
                this.activeConversation = undefined;
                break;
            }
        }
    };
    ChatComponent.prototype.createRoom = function (data) {
    };
    ChatComponent.prototype.createUserConversation = function (data) {
    };
    ChatComponent.prototype.makeStarred = function () {
        if (this.starreds.indexOf(this.activeConversation.data._id) < 0) {
            this.socketService.socket.emit('make-starred', { user_id: this.activeConversation.data._id });
        }
        else {
            this.socketService.socket.emit('make-unstarred', { user_id: this.activeConversation.data._id });
        }
    };
    ChatComponent.prototype.onSubmit = function () {
        if (this.message.trim().length > 0) {
            this.socketService.socket.emit('message', { message: this.message.trim(), type: this.activeConversation.type, _id: this.activeConversation.data._id });
            this.message = '';
        }
    };
    ChatComponent.prototype.findConversationIndex = function (params) {
        for (var i = 0; i < this.conversations.length; i++) {
            var c = this.conversations[i];
            if (c.type == params.type && c.data._id == params.data._id) {
                return i;
            }
        }
        return -1;
    };
    ChatComponent.prototype.findRoom = function (room_id) {
        for (var i = 0; i < this.chatRooms.length; i++) {
            if (this.chatRooms[i]._id == room_id) {
                return this.chatRooms[i];
            }
        }
        return null;
    };
    ChatComponent = __decorate([
        core_1.Component({
            selector: 'animagens-chat',
            templateUrl: '/app/chat/chat.template.html',
            styleUrls: ['app/chat/chat.style.css'],
            directives: [active_conversation_component_1.ActiveConversationComponent, list_component_1.ChatListComponent, angular2_auto_scroll_directive_1.Angular2AutoScroll]
        }),
        __param(5, core_1.Host()), 
        __metadata('design:paramtypes', [user_service_1.UsersService, socket_service_1.SocketService, core_2.NgZone, router_1.Router, platform_browser_1.Title, app_component_1.AppComponent])
    ], ChatComponent);
    return ChatComponent;
}());
exports.ChatComponent = ChatComponent;
//# sourceMappingURL=chat.component.js.map