"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_recaptcha_1 = require('angular2-recaptcha/angular2-recaptcha');
var socket_service_1 = require('../sockets/socket.service');
var nav_component_1 = require('../nav/nav.component');
var register_model_1 = require('./register.model');
var RegisterComponent = (function () {
    function RegisterComponent(socketService) {
        this.socketService = socketService;
        this.model = new register_model_1.Register('', '', '', '', '', '', '', 'unknown');
        this.submitted = false;
        this.successful = false;
        this.genderOptions = [
            { value: 'unknown', name: 'Sin especificar' },
            { value: 'male', name: 'Hombre' },
            { value: 'female', name: 'Mujer' }
        ];
        this.captcha_resolved = false;
        socketService.socket.emit('request-session', { session_token: socketService.session_token, page: location.pathname });
        socketService.socket.on('session', function (data) {
            if (data.user) {
                window.location.href = '/';
            }
        });
    }
    RegisterComponent.prototype.handleCorrectCaptcha = function (event) {
        this.captcha_resolved = true;
    };
    RegisterComponent.prototype.onSubmit = function () {
        var _this = this;
        this.error = null;
        if (!config.test && !this.captcha_resolved) {
            this.error = {
                type: 'danger',
                msg: 'Captcha erróneo'
            };
        }
        else if (this.model.password != this.model.rep_password) {
            this.error = {
                type: 'danger',
                msg: 'Las passwords no coinciden'
            };
        }
        else if (this.model.email != this.model.rep_email) {
            this.error = {
                type: 'danger',
                msg: 'Los correos electrónicos no coinciden'
            };
        }
        else {
            this.submitted = true;
            this.socketService.send('register', this.model, function (data) {
                _this.submitted = false;
                if (!data.err) {
                    _this.successful = true;
                }
                else if (data.err.code == 101) {
                    _this.error = {
                        type: 'danger',
                        msg: 'Ese nombre de usuario ya esta en uso'
                    };
                }
                else if (data.err.code == 102) {
                    _this.error = {
                        type: 'danger',
                        msg: 'Esa dirección de correo elctrónico ya esta en uso'
                    };
                }
                else {
                    _this.error = {
                        type: 'danger',
                        msg: 'Error desconocido. Por favor, contacta con nosotros en soporte@animagens.es para arreglarlo lo antes posible'
                    };
                }
            });
        }
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: 'animagens-register',
            templateUrl: '/app/register/register.template.html',
            styleUrls: ['app/register/register.style.css'],
            directives: [nav_component_1.NavComponent, angular2_recaptcha_1.ReCaptchaComponent],
            providers: [],
            animations: [
                core_1.trigger('postState', [
                    core_1.state('void', core_1.style({ opacity: 0, transform: 'translateY(+200px)' })),
                    core_1.state('in', core_1.style({ opacity: 1, transform: 'translateY(0px)' })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1,
                                transform: 'translateY(0px)'
                            }))
                        ])
                    ])
                ]),
                core_1.trigger('backgroundState', [
                    core_1.state('void', core_1.style({ opacity: 0 })),
                    core_1.state('in', core_1.style({ opacity: 1 })),
                    core_1.transition('void => *', [
                        core_1.group([
                            core_1.animate('0.5s 0.1s ease', core_1.style({
                                opacity: 1
                            }))
                        ])
                    ])
                ])
            ]
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map