import { Component, Inject, forwardRef, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';
import { Router } from '@angular/router';
import {ReCaptchaComponent} from 'angular2-recaptcha/angular2-recaptcha';

import { AppComponent } from '../app.component';
import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { UsersService } from '../users/user.service';
import { MenuItem } from '../nav/menu-item.model';
import { Login } from './login.model';

@Component({
    selector: 'animagens-login',
    templateUrl: '/app/login/login.template.html',
    styleUrls: ['app/login/login.style.css'],
    directives: [NavComponent, ReCaptchaComponent],
    providers: [],
    animations: [
        trigger('postState', [
            state('void', style({opacity: 0, transform: 'translateY(+200px)'})),
            state('in', style({opacity: 1, transform: 'translateY(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1,
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ]),
        trigger('backgroundState', [
            state('void', style({opacity: 0})),
            state('in', style({opacity: 1})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1
                    }))
                ])
            ])
        ])
    ]
})
export class LoginComponent {
    model = new Login('', '');

    submitted = false;
    successful = false;
    error: any;

    onSubmit() {
        this.error = null;

        this.submitted = true;
        this.socketService.send('request-login', this.model, (data:any) => {
            this.submitted = false;
            if (!data.err) {
                document.cookie = "session_token=" + data.session_token;
                this.socketService.session_token = data.session_token;
                this.router.navigate(['/']);   
            } else if (data.err.code == 109) {
                this.error = {
                    type: 'danger',
                    msg: 'El usuario y/o la contraseña introducidos no son correctos'
                };
            } else if (data.err.code == 110) {
                this.error = {
                    type: 'danger',
                    msg: 'El usuario introducido no ha sido verificado aún. Comprueba la bandeja de correo no deseado.'
                };
            } else {
                this.error = {
                    type: 'danger',
                    msg: 'Error desconocido. Por favor, contacta con nosotros en soporte@animagens.es para arreglarlo lo antes posible'
                };
            }
        });
       
    }

    constructor(private socketService: SocketService, private router: Router) {
        socketService.socket.emit('request-session', {session_token: socketService.session_token, page: location.pathname});
        socketService.socket.on('session', (data:any) => {
            if (data.user) {
                console.log('isLogin!!');
                router.navigate(['/']);
            }
        });
    }
}