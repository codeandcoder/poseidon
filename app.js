"use strict"
const fs           = require('fs'),
      path         = require('path'),
      contentTypes = require('./utils/content-types'),
      sysInfo      = require('./utils/sys-info'),
      env          = process.env,
      utils        = require('./node/utils'),
      db           = require('./node/db');

var cookieParser = require('cookie-parser')();
var session = require('cookie-session')({ secret: '11111' });

var lastFileName = 'error';
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, './static/images/users');
  },
  filename: function (req, file, callback) {
      if (req.url == '/uploadUserImage') {
        var session_token = utils.parseCookies(req.headers.cookie).session_token;
        db.checkSession(session_token, function(err, user) {
            if (err || !user) {
                callback({msg: 'No Session!'});
            } else {
                callback(null, user.nickname);
            }
        });
      } else {
        lastFileName = utils.generateRandomToken(64);
        callback(null, lastFileName);
      }
  }
});
var upload = multer({storage : storage}).single('file');

/*var options = {
    key  : fs.readFileSync('/root/animagens2/ssl/server.key'),
    cert : fs.readFileSync('/root/animagens2/ssl/cagd.crt'),
    ca : fs.readFileSync('/root/animagens2/ssl/bundlegd.crt')
};*/

let server = require('http').createServer(function (req, res) {
  let url = req.url;
  if (url == '/' || url == '/chat' || url == '/terms' || url.indexOf('/registro') > -1 || url.indexOf('/verificar') > -1 || url.indexOf('/entrar') > -1 || url.indexOf('/inicio') > -1 || url.indexOf('/foro') > -1 || url.indexOf('/sillon') > -1 || url.indexOf('/incognito') > -1 || url.indexOf('/tienda') > -1) {
    url = '/index.html';
  }

  // IMPORTANT: Your application HAS to respond to GET /health with status 200
  //            for OpenShift health monitoring
  if (url == '/uploadUserImage' || url == '/uploadImage') {
    upload(req, res, function (err) {
      if (err) {
        return res.end(err.toString());
      }
      res.end(lastFileName);
    });
  } else if (url == '/health') {
    res.writeHead(200);
    res.end();
  } else if (url == '/info/gen' || url == '/info/poll') {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Cache-Control', 'no-cache, no-store');
    res.end(JSON.stringify(sysInfo[url.slice(6)]()));
  } else {
    let root = url.split('?')[0];
    fs.readFile('./static' + root, function (err, data) {
      if (err) {
        res.writeHead(404);
        res.end('Not found');
      } else {
        let ext = path.extname(root).slice(1);
        if (contentTypes[ext]) {
          res.setHeader('Content-Type', contentTypes[ext]);
        }
        if (ext === 'html') {
          res.setHeader('Cache-Control', 'no-cache, no-store');
        }
        res.end(data);
      }
    });
  }
});

var http = require('express')();
    // set up a route to redirect http to https
    http.get('*',function(req,res){
        res.redirect('https://animagens.es'+req.url);
    });
    // have it listen on 3000
    http.listen(3000);


let io = require('socket.io')(server);
io.use(function(socket, next) {
    let req = socket.handshake;
    let res = {};
    cookieParser(req, res, function(err) {
        if (err) {
            return next(err);
        }
        session(req, res, next);
    });
});

server.listen(4000, 'localhost', function () {
  console.log(`Application worker ${process.pid} started...`);
});

require('./node/real-time').init(io);
require('./node/neo-checker').init();
