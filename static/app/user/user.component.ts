import { Component, Input, OnInit } from '@angular/core';
import { NgZone } from '@angular/core';
import { RouteParams } from '@angular/router-deprecated';
import { FORM_DIRECTIVES, FormBuilder, Validators } from '@angular/common';
import { FILE_UPLOAD_DIRECTIVES, FileUploader } from 'ng2-file-upload';

import { ConfigService } from '../config/config.service';
import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { MenuItem } from '../nav/menu-item.model';

@Component({
    selector: 'amgs-user',
    templateUrl: "app/user/user.template.html",
    styleUrls: ["app/user/user.style.css"],
    directives: [FORM_DIRECTIVES, FILE_UPLOAD_DIRECTIVES, NavComponent],
    providers: []
})
export class UserComponent {
    menuItems = [
        new MenuItem('Inicio', 'Home', 'fa-home', false),
        new MenuItem('Chat', 'Chat', 'fa-comments-o', false),
        new MenuItem('Foro', 'Forum', 'fa-globe', false)
    ];
    primaryColor = "darkblue";

    isEdit = false;
    user: any;
    imagesUrl = this.config.imagesUrl;
    uploader:FileUploader = new FileUploader({url: this.config.imagesUploadUrl});

    form: any;
    error: any;
    submitted = false;

    logout() {
        this.socketService.send('request-logout', {});
    }

    onSubmit(value: any) {
        value._id = this.user._id;
        if (this.uploader.queue[0]) {
            value.image = this.imagesUrl + '/' + this.user.nickname;
        } else {
            value.image = this.user.image;
        }
        this.socketService.send('update-user', value);
        this.submitted = true;
    }

    ngOnInit() {
        this.socketService.addListener('user-updated', (data: any) => {
            this.zone.run(() => {
                if (data.err) {
                    this.submitted = false;
                    this.error = {
                        type: 'danger',
                        msg: 'El desconocido. Por favor, contacta con nosotros en soporte@animagens.es'
                    };
                } else {
                    location.reload();
                }
            });
        });
        this.socketService.addListener('user-info', (user: any) => {
            this.zone.run(() => {
                this.user = user;
                if (this.user._id == this.socketService.myUser._id) {
                    this.isEdit = true;
                    this.form = this.fb.group({
                        alias: [this.user.alias, Validators.compose([Validators.required, Validators.minLength(4)])],
                        color: [this.user.color],
                        sound: [this.user.sound],
                        confidential: [this.user.confidential]
                    });
                }
            });
        });
        this.socketService.addListener('session', (data: any) => {
            if (data.err || !data.user) {
                document.location.href = '/login';
            } else {
                this.socketService.send('request-user', {nickname: this.params.get('u')});
            }
        });
        this.socketService.send('request-session', {session_token: this.socketService.session_token, page: location.pathname});
    }

    constructor(private socketService: SocketService, private params: RouteParams, private zone: NgZone, private fb: FormBuilder, private config: ConfigService) {
    }
}