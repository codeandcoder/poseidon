import { Injectable } from '@angular/core';

@Injectable()
export class SocketService {
    socket: any;
    session_token: string = "blabla";
    request_id: number = 0;
    listeners: any = {};

    send(operation: string, data: any, f: Function) {
        f(data);
    }
}