"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var socket_service_1 = require('../sockets/socket.service');
var VerificationComponent = (function () {
    function VerificationComponent(socketService, route) {
        this.socketService = socketService;
        this.route = route;
        this.successful = false;
    }
    VerificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socketService.socket.on('session', function (data) {
            if (data.err) {
            }
            else if (data.user) {
                document.location.href = '/';
            }
            else {
                _this.route.params.map(function (params) { return params['token']; }).subscribe(function (token) {
                    _this.socketService.send('request-verification', { token: token }, function (data) {
                        if (data.err) {
                            _this.error = {
                                type: 'danger',
                                msg: 'No se ha podido verificar tu cuenta. Por favor, ponte en contacto con nosotros en soporte@animagens.es'
                            };
                        }
                        else {
                            _this.successful = true;
                        }
                    });
                });
            }
        });
        this.socketService.socket.emit('request-session', { session_token: this.socketService.session_token, page: location.pathname });
    };
    VerificationComponent = __decorate([
        core_1.Component({
            selector: 'amgs-verification',
            templateUrl: 'app/verification/verification.template.html',
            styleUrls: ['app/verification/verification.style.css'],
            providers: [],
            directives: []
        }), 
        __metadata('design:paramtypes', [socket_service_1.SocketService, router_1.ActivatedRoute])
    ], VerificationComponent);
    return VerificationComponent;
}());
exports.VerificationComponent = VerificationComponent;
//# sourceMappingURL=verification.component.js.map