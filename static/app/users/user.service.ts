import { Injectable, Inject } from '@angular/core';

import { SocketService } from '../sockets/socket.service';

@Injectable()
export class UsersService {
    users = {};

    getUser(u_id: any, f: Function) {
        if (!this.users) this.users = {};
        if (!this.users[u_id]) {
            this.socketService.send('request-user', {user_id: u_id}, (data: any) => {
                this.users[data._id] = data;
                f(this.users[u_id]);
            });
        } else {
            f(this.users[u_id]);
        }
    }

    constructor(private socketService: SocketService) {
        socketService.socket.on('user-updated', (data: any) => {
            this.users[data.user._id] = data.user;
        });
    }
}