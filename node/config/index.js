var fs = require('fs');
var vars;

exports.vars = function(file) {
	if (vars === undefined) {
        if (file == undefined) {
            vars = JSON.parse(fs.readFileSync('config.json'));
        } else {
            vars = JSON.parse(fs.readFileSync(file));
        }
    }
	return vars;
};