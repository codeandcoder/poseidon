import { Component, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';
import {UPLOAD_DIRECTIVES} from 'ng2-uploader/ng2-uploader';
import {CKEditor} from 'ng2-ckeditor';

import { SocketService } from '../sockets/socket.service';
import { NavComponent } from '../nav/nav.component';
import { UsersService } from '../users/user.service';
import { MenuItem } from '../nav/menu-item.model';
import { Blog } from './blog.model';

@Component({
    selector: 'animagens-home-creation',
    templateUrl: '/app/home-creation/home-creation.template.html',
    styleUrls: ['app/home-creation/home-creation.style.css'],
    directives: [NavComponent, UPLOAD_DIRECTIVES, CKEditor],
    providers: [],
    animations: [
        trigger('postState', [
            state('void', style({opacity: 0, transform: 'translateY(+200px)'})),
            state('in', style({opacity: 1, transform: 'translateY(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1,
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ]),
        trigger('backgroundState', [
            state('void', style({opacity: 0})),
            state('in', style({opacity: 1})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        opacity: 1
                    }))
                ])
            ])
        ])
    ]
})
export class HomeCreationComponent {
    model = new Blog('images/dots.jpg', '', '');

    // Image Upload
    imagesUrl = config.imagesUrl;
    options: Object = {
        url: config.imagesUploadUrl
    };
    handleUpload(data: any): void {
        if (data && data.response) {
            this.model.image = 'images/users/' + data.response;
        }
    }

    // Form
    submitted = false;

    onSubmit() {
        this.submitted = true;
        this.socketService.send('create-blog', this.model, (data:any) => {
            window.location.href = '/';
        });
    }

    constructor(private socketService: SocketService, private zone: NgZone) {

    }
}