import { Component, Input, OnChanges, Output, EventEmitter } from '@angular/core';

import {UsersService} from '../../users/user.service';

@Component({
    selector: 'amgs-active-conversation',
    templateUrl: 'app/chat/conversation/active-conversation.template.html',
    styleUrls: ['app/chat/conversation/active-conversation.style.css']
})
export class ActiveConversationComponent implements OnChanges {
    users = this.usersService.users;

    @Input()
    conversation: any;
    @Input()
    chatUsers: any;
    @Input()
    starreds: any[] = [];

    @Output()
    makeStarred = new EventEmitter();

    starred() {
        this.makeStarred.next({});
    }

    ngOnChanges(changes: any) {
      console.log(changes);
    }

    constructor(private usersService: UsersService) {
    }
}