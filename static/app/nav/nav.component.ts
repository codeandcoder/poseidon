import { Component, Input, Output, OnInit, trigger, state, style, transition, animate, group } from '@angular/core';
import { NgZone } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap/index';

import { SocketService } from '../sockets/socket.service';
import { MenuItem } from './menu-item.model';

import { CustomModalContext, StatusChangeModal } from './status-change.modal';

@Component({
    selector: 'animagens-nav',
    templateUrl: '/app/nav/nav.template.html',
    styleUrls: ['app/nav/nav.style.css'],
    directives: [],
    providers: [],
    animations: [
        trigger('navbarState', [
            state('void', style({transform: 'translateY(-150px)'})),
            state('in', style({transform: 'translateX(0px)'})),
            transition('void => *', [
                group([
                    animate('0.5s 0.1s ease', style({
                        transform: 'translateY(0px)'
                    }))
                ])
            ])
        ])
    ]
})
export class NavComponent {
    @Input()
    public user: any;

    @Input()
    public pageColor: string;

    @Input()
    public menuItems: MenuItem[];
    
    public myChatUser: any;
    public pendingMessages = 0;
    statuses = [
        'connected',
        'navigating',
        'bussy',
        'disconnected'
    ];
    colors = {
        'connected': 'green',
        'navigating': 'gold',
        'bussy': 'red',
        'disconnected': 'grey'
    };

    getStatusTitle() {
        if (this.myChatUser) {
            switch(this.myChatUser.status) {
                case 'connected':
                    return 'Conectado';
                case 'navigating':
                    return 'Navegando';
                case 'bussy':
                    return 'Ocupado';
                case 'disconnected':
                    return 'Desconectado';        
            }
        }
        return '';
    }

    openStatusModal() {
        return this.modal.open(StatusChangeModal,  overlayConfigFactory({}, BSModalContext));
    }

    logout() {
        this.socketService.socket.emit('request-logout');
    }

    constructor(private socketService: SocketService, private zone: NgZone, router:Router, title:Title, public modal: Modal, public overlay: Overlay) {
        router.events.subscribe((event:any) => {
            if(event instanceof NavigationEnd) {
                socketService.socket.on('session', (data:any) => {
                    this.socketService.socket.emit('request-chat-user', {});
                });
                this.socketService.socket.on('chat-user', (data:any) => {
                    if (data.err) {
                        this.myChatUser = {
                            status: 'navigating'
                        };
                    } else {
                        this.myChatUser = data.chatUser;
                    }
                });
                this.socketService.socket.on('pending-messages', (data:any) => {
                    this.pendingMessages = data.count;
                    if (this.pendingMessages > 0) {
                        title.setTitle('(' + this.pendingMessages + ') Animagens');
                    } else {
                        title.setTitle('Animagens');
                    }
                });
            }
        });
    }
}